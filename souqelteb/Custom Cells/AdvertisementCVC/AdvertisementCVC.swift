//
//  AdvertisementCVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/13/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class AdvertisementCVC: UICollectionViewCell {
    // MARK:- Outlets
    @IBOutlet weak var advertiseImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureAdvertise(ads: Advertise) {
        guard let img = ads.img?[0] else{return}
        self.advertiseImageView.setImage(imageUrl: img)
    }
    
}
