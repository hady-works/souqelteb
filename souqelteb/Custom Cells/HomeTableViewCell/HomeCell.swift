//
//  HomeCell.swift
//  souqelteb
//
//  Created by HadyHammad on 12/30/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell{

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var productCollectionView: UICollectionView!
    @IBOutlet weak var sectionTitle: UILabel!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var emptyLbl: UILabel!
    
    var offersArray         = [Product]()
    var topSellerArray      = [Product]()
    var firstCategoryArray  = [Product]()
    var secondCategoryArray = [Product]()
    var delegate:ProductDetailDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        pageControl.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
        self.productCollectionView.delegate = self
        self.productCollectionView.dataSource = self
    }
}

extension HomeCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        let cellIndex = Int((x+190 / 2) / 190)
        self.pageControl.currentPage = cellIndex
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 1:
            return offersArray.count
        case 2:
            return topSellerArray.count
        case 3:
            if firstCategoryArray.isEmpty{
                return secondCategoryArray.count
            }else{
                return firstCategoryArray.count
            }
        case 4:
            return secondCategoryArray.count
        default:
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(indexPath: indexPath) as ProductCVCell
        switch collectionView.tag {
        case 1:
            cell.configure(product: self.offersArray[indexPath.row])
            cell.favouriteBtn.isUserInteractionEnabled = false
            return cell
        case 2:
            cell.configure(product: self.topSellerArray[indexPath.row])
            cell.favouriteBtn.isUserInteractionEnabled = false
            return cell
        case 3:
            if self.firstCategoryArray.isEmpty{
                cell.configure(product: self.secondCategoryArray[indexPath.row])
                cell.favouriteBtn.isUserInteractionEnabled = false
                return cell
            }else{
                cell.configure(product: self.firstCategoryArray[indexPath.row])
                cell.favouriteBtn.isUserInteractionEnabled = false
                return cell
            }
        case 4:
            cell.configure(product: self.secondCategoryArray[indexPath.row])
            cell.favouriteBtn.isUserInteractionEnabled = false
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width: 170, height: 220)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView.tag {
        case 1:
            delegate.goToDetails(product: self.offersArray[indexPath.row])
        case 2:
            delegate.goToDetails(product: self.topSellerArray[indexPath.row])
        case 3:
            if self.firstCategoryArray.isEmpty{
                delegate.goToDetails(product: self.secondCategoryArray[indexPath.row])
            }else{
                delegate.goToDetails(product: self.firstCategoryArray[indexPath.row])
            }
        case 4:
            delegate.goToDetails(product: self.secondCategoryArray[indexPath.row])
        default:
            print("default")
        }
    }
}
