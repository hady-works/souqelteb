//
//  AdvertisementTVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/13/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class AdvertisementTVC: UITableViewCell{
    
    // MARK:- Outlets
    @IBOutlet weak var advertisementCV: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    // MARK:- Instance Variables
    var advertiseArray = [Advertise]()
    var selectedIndex = 0
    var timer = Timer()
    var counter = 0
    
    // MARK:- LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        pageControl.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
        self.advertisementCV.delegate = self
        self.advertisementCV.dataSource = self
        setupComponents()
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        }
    }
    
    @objc func changeImage() {
        if counter < advertiseArray.count {
            self.pageControl.currentPage = counter
            let index = IndexPath.init(item: counter, section: 0)
            self.advertisementCV.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            counter += 1
        } else {
            if advertiseArray.count > 0{
                counter = 0
                self.pageControl.currentPage = counter
                let index = IndexPath.init(item: counter, section: 0)
                self.advertisementCV.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
                counter = 1
            }
        }
        
    }
    
}

extension AdvertisementTVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offestX = scrollView.contentOffset.x
        let width = self.advertisementCV.frame.width
        let cellIndex = Int((offestX + width / 2) / width)
        self.selectedIndex = cellIndex
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.advertiseArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(indexPath: indexPath) as AdvertisementCVC
        cell.configureAdvertise(ads: self.advertiseArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
}
