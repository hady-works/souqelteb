//
//  ProductDetailsCollectionViewCell.swift
//  souqelteb
//
//  Created by HadyHammad on 1/11/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class ProductDetailsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configure(imgURL: String?) {
        guard let imgURL     = imgURL else{return}
        DispatchQueue.main.async {
            self.img.setImage(imageUrl: imgURL)
        }
    }
}
