//
//  OrderMainTVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/21/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import MOLH

class OrderMainTVC: UITableViewCell {
    
    @IBOutlet weak var ordersTV: UITableView!
    @IBOutlet weak var orderSummaryView: UIView!
    @IBOutlet weak var orderNumberLbl: UILabel!
    @IBOutlet weak var orderPriceLbl: UILabel!
    @IBOutlet weak var statusImg: UIImageView!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    
    
    var orderProductArray:[ProductOrders]? {
        didSet{
            self.ordersTV.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    // MARK:- SetupUI
    func setupUI(){
        ordersTV.delegate = self
        ordersTV.dataSource = self
        ordersTV.registerCellNib(cellClass: OrderTVC.self)
        orderSummaryView.roundedFromSide(corners: [.topLeft, .topRight], cornerRadius: 8)
        orderSummaryView.addNormalShadow()
    }
    
    func configure(order: Order?){
        self.orderProductArray = order?.productOrders
        guard let orderNumber = order?.id,
            let address = order?.address,
            let price   = order?.finalTotal,
            let status  = order?.status else{return}
        
        if status == "COMPLETED"{
            self.statusImg.isHidden = false
        }else{
            self.statusImg.isHidden = true
        }
        self.orderNumberLbl.text = "\(orderNumber)"
        self.orderPriceLbl.text = MOLHLanguage.isArabic() ?  "\(price) جنيه" : "\(price) EGP"
        self.addressLbl.text = "\(address)"
        self.statusLbl.text = "\(status)"
    }
    
}

extension OrderMainTVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderProductArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ordersTV.dequeue() as OrderTVC
        if let productOrders = self.orderProductArray {
            cell.setupComponents()
            cell.configure(order: productOrders[indexPath.row])
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
}


