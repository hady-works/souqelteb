//
//  OrderTVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/15/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class OrderTVC: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productCompanyLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var cellViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK:- SetupUI
    func setupSummaryComponent(){
        cellView.addCornerRadius(10)
        cellView.addBorderWith(width: 1.5, color: UIColor.borderColor)
        cellView.addNormalShadow()
        cellViewBottomConstraint.constant = 8
        leadingConstraint.constant = 16
        trailingConstraint.constant = 16
    }
    
    func setupComponents(){
        cellView.addBorderWith(width: 1, color: UIColor.borderColor)
        cellView.addCornerRadius(5)
        cellViewBottomConstraint.constant = 0
        leadingConstraint.constant = 0
        trailingConstraint.constant = 0
    }
    
    func configure(order: ProductOrders?) {

        guard let count     = order?.count,
            let product     = order?.product,
            let offer       = product.hasOffer,
            let offerPrice  = product.offerPrice,
            let price       = product.price else{return}
        
        self.productNameLbl.text = product.name
        self.productCompanyLbl.text = product.company
        self.quantityLbl.text = "\(count)"
        if let img = product.img?.first{
            DispatchQueue.main.async {
                self.productImg.setImage(imageUrl: img)
            }
        }
        if offer{
            self.priceLbl.text = "\(offerPrice)"
        }else{
            self.priceLbl.text = "\(price)"
        }
        
    }
}
