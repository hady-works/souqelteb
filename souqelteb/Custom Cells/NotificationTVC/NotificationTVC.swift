//
//  NotificationTVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/26/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import MOLH

class NotificationTVC: UITableViewCell {
    
    @IBOutlet weak var notificationCell: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var messageTxtView: UITextView!
    @IBOutlet weak var notificationImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupComponents()
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        notificationCell.addCornerRadius(8)
        notificationCell.addBorderWith(width: 1.5, color: UIColor.borderColor)
        notificationCell.addNormalShadow()
    }
    
    func configure(notify: Notification?){
        guard let read = notify?.read else{return}
        guard let date = notify?.createdAt else{return}
        setupDate(stringDate: date)
        self.notificationCell.backgroundColor = read ? .white : UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        if let fname = notify?.resource?.firstname, let lname = notify?.resource?.lastname{
            self.nameLbl.text = fname + " " + lname
        }
        self.messageTxtView.text = MOLHLanguage.isArabic() ?  notify?.arabicDescription : notify?.description
        DispatchQueue.main.async {
            self.notificationImage.setImage(imageUrl: notify?.resource?.img ?? "")
        }
    }
    
    func setupDate(stringDate: String){
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC") //TimeZone.current//
        dateFormatter.locale = Locale(identifier: "en_US")
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        if let date = dateFormatter.date(from: stringDate) {
            dateFormatter.dateFormat = "dd/MM/yyyy h:mm a"
            self.dateLbl.text = "\(dateFormatter.string(from: date))"
        }
    }
    
}
