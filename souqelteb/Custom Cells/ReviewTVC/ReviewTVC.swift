//
//  ReviewTVC.swift
//  souqelteb
//
//  Created by Hady Hammad on 2/12/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Cosmos

class ReviewTVC: UITableViewCell {

    // MARK:- Outlets
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var commentTxtView: UITextView!
    
    // MARK:- LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupComponents()
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        commentView.addCornerRadius(10)
        userImg.addCornerRadius(userImg.frame.height / 2)
    }
    
    func configure(review: Review?) {
        guard let userName = review?.user?.firstname else{return}
        guard let rateString = review?.rate else{return}
        guard let rate = Double(rateString) else{return}
        guard let comment = review?.comment else{return}
        guard let userImage = review?.user?.img else{return}
        DispatchQueue.main.async {
            self.userImg.setImage(imageUrl: userImage)
            self.userNameLbl.text = userName
            self.rateView.rating = rate
            self.commentTxtView.text = comment
        }
    }
}
