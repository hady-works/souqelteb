//
//  CategoryCollectionViewCell.swift
//  souqelteb
//
//  Created by HadyHammad on 12/27/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import MOLH

class CategoryCollectionViewCell: UICollectionViewCell {

    // MARK:- Outlets
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var categoryNameLbl: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupComponents()
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        categoryView.addCornerRadius(15)
        categoryView.addBorderWith(width: 0.5, color: UIColor.borderColor)
        categoryImageView.addCornerRadius(20)
    }
    
    func configure(category: Category?) {
        if let categoryName = category?.categoryname, let categoryArabicName = category?.arabicname, let img = category?.img {
            self.categoryNameLbl.text  = MOLHLanguage.isArabic() ?  categoryArabicName : categoryName
            DispatchQueue.main.async {
                self.categoryImageView.setImage(imageUrl: img)
            }
        }
    }

}
