//
//  CartTVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/6/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import MOLH

class CartTVC: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productCompanyLbl: UILabel!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var numberOfItemsLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var oldPriceView: UIView!
    @IBOutlet weak var oldPriceLbl: UILabel!
    @IBOutlet weak var saleView: UIView!
    @IBOutlet weak var saleLbl: UILabel!
    @IBOutlet weak var removeFromCartBtn: UIButton!
    @IBOutlet weak var plusView: UIView!
    @IBOutlet weak var minusView: UIView!
    
    //MARK:- Variables
    var product:Product?
    var delegate:ChangePriceDelegate?
    
    //MARK:- LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupComponents()
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        cellView.addCornerRadius(10)
        cellView.addBorderWith(width: 1.5, color: UIColor.borderColor)
        cellView.addNormalShadow()
    }
    
    func configure(product: Product?){
        self.product = product
        guard let product = product else{return}
        guard let offer       = product.hasOffer else{return}
        guard let offerPrice  = product.offerPrice else{return}
        guard let price       = product.price else{return}
        guard let sale       = product.offerRatio else{return}
        self.productNameLbl.text = product.name
        self.productCompanyLbl.text = product.company
        if let img = product.img?[0]{
            DispatchQueue.main.async {
                self.productImg.setImage(imageUrl: img)
            }
        }
        setupPrice(offer: offer, offerPrice: offerPrice, sale: sale, price: price)
    }
    
    func setupPrice(offer: Bool, offerPrice: Double, sale: Int, price: Double){
        if offer{
            self.oldPriceView.isHidden = false
            self.saleView.isHidden = false
            self.priceLbl.text = "\(offerPrice)"
            self.oldPriceLbl.text = MOLHLanguage.isArabic() ?  "\(price) جنيه" : "\(price) EGP"
            self.saleLbl.text = "\(sale)%"
        }else{
            self.oldPriceView.isHidden = true
            self.saleView.isHidden = true
            self.priceLbl.text = "\(price)"
        }
    }
    
    //MARK:- Actions
    @IBAction func buIncreaseItems(_ sender: Any) {
        guard let id = self.product?.id, let stockQuantity = self.product?.quantity else{return}
        guard var value    = Int(self.numberOfItemsLbl.text!) else{return}
        value = value + 1
        if value <= stockQuantity{
            delegate?.changeQuantity(id: id, Qty: value, warning: false)
            self.numberOfItemsLbl.text = "\(value)"
        }else{
            delegate?.changeQuantity(id: id, Qty: stockQuantity, warning: true)
        }
    }
    
    @IBAction func buDecreaseItems(_ sender: Any) {
        guard let id = self.product?.id else{return}
        guard var value    = Int(self.numberOfItemsLbl.text!) else{return}
        if value > 1{
            value = value - 1
            delegate?.changeQuantity(id: id, Qty: value, warning: false)
            self.numberOfItemsLbl.text = "\(value)"
        }
    }
    
}
