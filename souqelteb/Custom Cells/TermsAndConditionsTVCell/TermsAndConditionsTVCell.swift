//
//  TermsAndConditionsTVCell.swift
//  souqelteb
//
//  Created by Mac on 12/13/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class TermsAndConditionsTVCell: UITableViewCell {

    // MARK:- Outlets
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(model: TermsModel) {
            self.titleLbl.text = model.title
            self.detailLbl.text = model.details
    }
    
}
