//
//  ProductCVCell.swift
//  souqelteb
//
//  Created by HadyHammad on 12/30/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import Cosmos
import MOLH

class ProductCVCell: UICollectionViewCell {
    
    // MARK:- Outlets
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var cartBtn: UIButton!
    @IBOutlet weak var saleImageView: UIImageView!
    @IBOutlet weak var saleLbl: UILabel!
    @IBOutlet weak var starRateView: CosmosView!
    @IBOutlet weak var oldPriceView: UIView!
    @IBOutlet weak var oldPriceLbl: UILabel!
    @IBOutlet weak var saleView: UIView!
    
    // MARK:- Variables
    var product:Product?
    var cartArray = NetworkHelper.getCart()
    var favouriteArray = NetworkHelper.getFavourite()
    var delegate:RemoveFavouriteDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupComponents()
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        cellView.addCornerRadius(5)
        cellView.addBorderWith(width: 1.5, color: UIColor.borderColor)
        cellView.addShadowWith(color: UIColor.shadowColor, radius: 3, opacity: 0.6)
    }
    
    func configure(product: Product?) {
        self.cellView.isHidden = false
        guard let product     = product else{return}
        self.product          = product
        guard let id          = product.id else{return}
        guard let offer       = product.hasOffer else{return}
        guard let offerPrice  = product.offerPrice else{return}
        guard let price       = product.price else{return}
        guard let sale        = product.offerRatio else{return}
        guard let ratePrecent = product.ratePrecent else{return}
        
        if let img = product.img?[0]{
            DispatchQueue.main.async {
                self.productImageView.setImage(imageUrl: img)
            }
        }
        
        setupFavouriteAndCart(productID: id)
        setupPrice(offer: offer, offerPrice: offerPrice, sale: sale, price: price)
        self.productNameLbl.text = product.name
        self.starRateView.rating = ratePrecent/20
    }
    
    func setupFavouriteAndCart(productID: Int){
        
        if (cartArray.contains(productID)){
            cartBtn.setImage(UIImage(named: "local_grocery_store"), for: .normal)
        }else{
            cartBtn.setImage(UIImage(named: "add_shopping_cart"), for: .normal)
        }
        
        if (favouriteArray.contains(productID)){
            favouriteBtn.setImage(UIImage(named: "favorite-1"), for: .normal)
        }else{
            favouriteBtn.setImage(UIImage(named: "favorite"), for: .normal)
        }
        
    }
    
    func setupPrice(offer: Bool, offerPrice: Double, sale: Int, price: Double){
        if offer{
            self.oldPriceView.isHidden = false
            self.saleView.isHidden = false
            self.priceLbl.text = "\(offerPrice)"
            self.oldPriceLbl.text = MOLHLanguage.isArabic() ?  "\(price) جنيه" : "\(price) EGP"
            self.saleLbl.text = "\(sale)%"
        }else{
            self.oldPriceView.isHidden = true
            self.saleView.isHidden = true
            self.priceLbl.text = "\(price)"
        }
    }
    
    @IBAction func buFavourite(_ sender: Any) {
        if let _ = NetworkHelper.getAccessToken(), let productID = self.product?.id{
            if !(self.favouriteArray.contains(productID)){
                addToFavourite(id: productID)
            }else{
                removeFromFavourite(id: productID)
            }
        }
    }
    
    func addToFavourite(id: Int){
        favouriteBtn.setImage(UIImage(named: "favorite-1"), for: .normal)
        API.sendRequest(method: .post, url: Constants.favourite+"\(id)/products", header: Constants.header, completion: { (err,status,response: Response?) in
            if err == nil{
                if status!{
                    guard let arr = response?.user?.favourite else{return}
                    NetworkHelper.favourite = arr
                    self.favouriteArray = arr
                }
            }
        })
    }
    
    func removeFromFavourite(id: Int){
        favouriteBtn.setImage(UIImage(named: "favorite"), for: .normal)
        API.sendRequest(method: .delete, url: Constants.favourite+"\(id)", header: Constants.header, completion: { (err,status,response: Response?) in
            if err == nil{
                if status!{
                    guard let arr = response?.user?.favourite else{return}
                    NetworkHelper.favourite = arr
                    self.favouriteArray = arr
                    if self.delegate != nil{
                        self.delegate?.updateCollection(id: id)
                    }
                }
            }
        })
    }
    
}
