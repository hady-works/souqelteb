//
//  Payfort.swift
//  souqelteb
//
//  Created by HadyHammad on 1/20/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

struct Payfort:Decodable {
    var command : String?
    var merchant_reference :String?
    var sdk_token : String?
    var amount :String?
    var currency :String?
    var customer_email :String?
    var payment_option :String?
    var eci :String?
    var order_description :String?
    var customer_ip :String?
    var customer_name :String?
    var settlement_reference :String?
    var merchant_extra :String?
    var merchant_extra1 :String?
    var merchant_extra2 :String?
    var merchant_extra3 :String?
    var merchant_extra4 :String?
    var fort_id :String?
    var authorization_code :String?
    var response_message :String?
    var response_code :String?
    var expiry_date :String?
    var card_number :String?
    var status :String?
    var phone_number :String?
    var token_name:String?
}
