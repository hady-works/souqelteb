//
//  Introduction.swift
//  souqelteb
//
//  Created by HadyHammad on 12/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

struct Introduction{
    var image:UIImage
    var text:String
    
    init(image:UIImage,text:String) {
        self.image = image
        self.text = text
    }
}
