//
//  Order.swift
//  souqelteb
//
//  Created by HadyHammad on 1/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

struct OrdersResponse:Decodable {
    var data:[Order]?
    var page:Int?
    var pageCount:Int?
    var limit:Int?
    var totalCount:Int?
}

struct Order:Decodable {
    var finalTotal: Double?
    var status: String?
    var paymentSystem: String?
    var accept: Bool?
    var deleted: Bool?
    var address: String?
    var city: City?
    var area: Area?
    var productOrders: [ProductOrders]?
    var total: Double?
    var deliverayCost: Double?
    var client: User2?
    var id: Int?
}

struct ProductOrders:Decodable {
    var count:Int?
    var product:Product?
}
