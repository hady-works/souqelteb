//
//  Product.swift
//  souqelteb
//
//  Created by HadyHammad on 12/30/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

struct ProductResponse:Decodable {
    var data:[Product]?
    var page:Int?
    var pageCount:Int?
    var limit:Int?
    var totalCount:Int?
}

struct Product:Decodable {
    var img:[String]?
    var hasOffer:Bool?
    var offerRatio:Int?
    var offerPrice:Double?
    var rate:Double?
    var rateNumbers:Int?
    var ratePrecent:Double?
    var visible:Bool?
    var available:Bool?
    var sallCount:Int?
    var top:Bool?
    var deleted:Bool?
    var name:String?
    var company:String?
    var description:String?
    var price:Double?
    var deliveryCostBySize:Int?
    var quantity:Int?
    var hasColors:Bool?
    var hasSizes:Bool?
    var color:[String]?
    var sizes:[String]?
    var category:Category?
    var id:Int?
}
