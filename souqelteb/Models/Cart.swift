//
//  Cart.swift
//  souqelteb
//
//  Created by HadyHammad on 1/10/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
struct CartResponse:Decodable{
    var data:[Cart]?
}

struct Cart:Decodable{
    var deleted:Bool?
    var user:User2?
    var product:Product?
    var id:Int?
    var color:String?
    var size:String?
}
