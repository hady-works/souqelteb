//
//  Review.swift
//  souqelteb
//
//  Created by HadyHammad on 1/25/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
struct ReviewResponse:Decodable{
    var data:[Review]?
}

struct Review:Decodable{
    var deleted:Bool?
    var user:User2?
    var comment:String?
    var rate:String?
    var product:Int?
    var id:Int?
}
