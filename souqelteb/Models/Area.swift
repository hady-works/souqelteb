//
//  Area.swift
//  souqelteb
//
//  Created by HadyHammad on 12/9/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

struct Area:Decodable {
    var areaName:String?
    var arabicAreaName:String?
    var city:Int?
    var id:Int?
}
