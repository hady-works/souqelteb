//
//  User.swift
//  souqelteb
//
//  Created by HadyHammad on 12/19/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

struct Response:Decodable {
    let user:User?
}

struct UpdateResponse:Decodable {
    let user:User2?
}

struct LoginResponse:Decodable {
    let user:User?
    let token:String?
}

struct User:Decodable {
    let id:Int?
    let favourite:[Int]?
    let carts:[Int]?
    let firstname:String?
    let lastname:String?
    let area:Area?
    let city:City?
    let address:String?
    let phone:String?
    let email:String?
    let type:String?
    let img:String?
}

struct User2:Decodable {
    let id:Int?
    let favourite:[Int]?
    let carts:[Int]?
    let firstname:String?
    let lastname:String?
    let area:Int?
    let city:Int?
    let address:String?
    let phone:String?
    let email:String?
    let type:String?
    let img:String?
}
