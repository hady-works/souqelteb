//
//  Favourite.swift
//  souqelteb
//
//  Created by HadyHammad on 1/6/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
struct FavouriteResponse:Decodable{
    var data:[Favourite]?
    var page:Int?
    var pageCount:Int?
    var limit:Int?
    var totalCount:Int?
}

struct Favourite:Decodable{
    var deleted:Bool?
    var user:User2?
    var product:Product?
    var id:Int?
}
