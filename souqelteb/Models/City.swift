//
//  City.swift
//  souqelteb
//
//  Created by HadyHammad on 12/9/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

struct City:Decodable {
    var cityName:String?
    var arabicCityName:String?
    var delivaryCost:Int?
    var id:Int?
}
