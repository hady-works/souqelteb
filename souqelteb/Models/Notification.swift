//
//  Notification.swift
//  souqelteb
//
//  Created by HadyHammad on 1/26/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

struct NotificationResponse:Decodable {
    var data:[Notification]?
    var page:Int?
    var pageCount:Int?
    var limit:Int?
    var totalCount:Int?
}

struct Notification:Decodable {
    var read:Bool?
    var deleted:Bool?
    var description:String?
    var arabicDescription:String?
    var resource:User2?
    var createdAt:String?
    var updatedAt:String?
    var id:Int?
}
