//
//  Advertise.swift
//  souqelteb
//
//  Created by HadyHammad on 1/5/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

struct AdevrtiseResponse:Decodable {
    var data:[Advertise]?
}

struct Advertise:Decodable {
    var img:[String]?
    var visible:Bool?
    var top:Bool?
    var deleted:Bool?
    var description:String?
    var id:Int?
}
