//
//  Category.swift
//  souqelteb
//
//  Created by HadyHammad on 12/27/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

struct CategoryResponse:Decodable {
    var data:[Category]?
}
struct Category:Decodable {
    var categoryname:String?
    var arabicname:String?
    var img:String?
    var id:Int?
}
