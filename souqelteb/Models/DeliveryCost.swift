//
//  DeliveryCost.swift
//  souqelteb
//
//  Created by Hady Hammad on 5/16/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
struct DeliveryCost:Decodable {
    var productsCost: Double?
    var delivaryCost: Double?
    var finalTotal:Double?
}
