//
//  TermsAndConditionsModel.swift
//  souqelteb
//
//  Created by Mac on 12/14/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import ObjectMapper

struct TermsAndConditionsModel : Mappable {
    
    var deleted : Bool?
    var about : String?
    var usage : String?
    var createdAt : String?
    var updatedAt : String?
    var conditions : String?
    var privacy : String?
    var id : Int?
    
    init?(map: Map) {}
    init?() {}
    
    mutating func mapping(map: Map) {
        
        deleted <- map["deleted"]
        about <- map["about"]
        usage <- map["usage"]
        createdAt <- map["createdAt"]
        updatedAt <- map["updatedAt"]
        conditions <- map["conditions"]
        privacy <- map["privacy"]
        id <- map["id"]
    }
    
    static func DTO(data: JSON) -> TermsAndConditionsModel{
        return Mapper<TermsAndConditionsModel>().map(JSON: data)!
    }
    
}
