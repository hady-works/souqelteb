//
//  ChangePasswordVC.swift
//  souqelteb
//
//  Created by Mac on 12/13/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class ChangePasswordVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> ChangePasswordVC{
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var currentPassowordView: UIView!
    @IBOutlet weak var currentTxtField: UITextField!
    @IBOutlet weak var currentVisionBtn: UIButton!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var newTxtField: UITextField!
    @IBOutlet weak var newVisionBtn: UIButton!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var confirmTxtField: UITextField!
    @IBOutlet weak var confirmVisionBtn: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var saveChanges: UIButton!
    
    // MARK:- Instance Variables
    var currentPasswordSecure = true
    var passwordSecure = true
    var confirmSecure = true
    var user:User?
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    override func viewDidLayoutSubviews() {
        self.topView.roundedFromSide(corners: [.bottomLeft, .bottomRight], cornerRadius: 12)
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        guard let img = self.user?.img else{return}
        self.userImg.setImage(imageUrl: img)
        currentTxtField.delegate = self
        newTxtField.delegate = self
        confirmTxtField.delegate = self
        currentPassowordView.addBorderWith(width: 1.5, color:  UIColor.borderColor)
        currentPassowordView.addCornerRadius(8)
        newPasswordView.addBorderWith(width: 1.5, color:  UIColor.borderColor)
        newPasswordView.addCornerRadius(8)
        confirmPasswordView.addBorderWith(width: 1.5, color:  UIColor.borderColor)
        confirmPasswordView.addCornerRadius(8)
        saveChanges.addBtnCornerRadius(8)
        saveChanges.addBtnShadowWith(color: UIColor.black, radius: 2, opacity: 0.2)
        topView.addShadowWith(color: UIColor.black, radius: 5, opacity: 0.6)
    }
    
    // MARK:- Action
    @IBAction func saveChanges(_ sender: Any) {
        if validData(){
            let parameter =  ["currentPassword": currentTxtField.text!, "newPassword": newTxtField.text!]
            API.sendRequest(method: .put, url: Constants.update_password, parameters: parameter, header: Constants.header, completion: { (err,status,response: UpdateResponse?)in
                if err == nil{
                    if status!{
                        self.showAlertsuccess(title: "Password change success".localized)
                        self.finish()
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        self.showAlertWiring(title: "Password change faild".localized)
                    }
                }else{
                    self.showAlertError(title: "There is error may be connection lost".localized)
                }
            })
        }
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showCurrentPassword(_ sender: Any) {
        if currentPasswordSecure{
            switchSecureEntry(secure: currentPasswordSecure, passwordField: currentTxtField, visibility: currentVisionBtn)
            currentPasswordSecure = false
        }else{
            switchSecureEntry(secure: currentPasswordSecure, passwordField: currentTxtField, visibility: currentVisionBtn)
            currentPasswordSecure = true
        }
    }
    
    @IBAction func showPassword(_ sender: Any) {
        if passwordSecure{
            switchSecureEntry(secure: passwordSecure, passwordField: newTxtField, visibility: newVisionBtn)
            passwordSecure = false
        }else{
            switchSecureEntry(secure: passwordSecure, passwordField: newTxtField, visibility: newVisionBtn)
            passwordSecure = true
        }
    }
    
    @IBAction func showConfirmedPassword(_ sender: Any) {
        if confirmSecure{
            switchSecureEntry(secure: confirmSecure, passwordField: confirmTxtField, visibility: confirmVisionBtn)
            confirmSecure = false
        }else{
            switchSecureEntry(secure: confirmSecure, passwordField: confirmTxtField, visibility: confirmVisionBtn)
            confirmSecure = true
        }
    }
    
    // MARK:- Data Validations
    func validData() -> Bool{
        
        if currentTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter current password".localized)
            return false
        }
        
        if currentTxtField.text!.count <= 5{
            self.showAlertWiring(title: "Password is less than 6".localized)
            return false
        }
        
        if newTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter new password".localized)
            return false
        }
        
        if newTxtField.text!.count <= 5{
            self.showAlertWiring(title: "Password is less than 6".localized)
            return false
        }
        
        if confirmTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter confirm password".localized)
            return false
        }
        
        if confirmTxtField.text!.count <= 5{
            self.showAlertWiring(title: "Password is less than 6".localized)
            return false
        }
        
        if newTxtField.text != confirmTxtField.text{
            self.showAlertWiring(title: "Confirm password not match".localized)
            return false
        }
        
        return true
    }
    
    func finish(){
        self.currentTxtField.text = ""
        self.newTxtField.text = ""
        self.confirmTxtField.text = ""
    }
    
}

extension ChangePasswordVC: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == newTxtField{
            newPasswordView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }else if textField == confirmTxtField{
            confirmPasswordView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }else if textField == currentTxtField{
            currentPassowordView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == newTxtField{
            newPasswordView.layer.borderColor = UIColor.borderColor.cgColor
        }else if textField == confirmTxtField{
            confirmPasswordView.layer.borderColor = UIColor.borderColor.cgColor
        }else if textField == currentTxtField{
            currentPassowordView.layer.borderColor = UIColor.borderColor.cgColor
        }
    }
    
}
