//
//  EditProfileVC.swift
//  souqelteb
//
//  Created by Mac on 12/13/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import MOLH

class EditProfileVC: BaseViewController ,UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    // MARK:- Instance
    static func instance () -> EditProfileVC{
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userImgView: UIView!
    @IBOutlet weak var cameraEditBtn: UIButton!
    @IBOutlet weak var firstnameTxtField: CustomTxtField!
    @IBOutlet weak var lastnameTxtField: CustomTxtField!
    @IBOutlet weak var emailTxtField: CustomTxtField!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var cityTxtField: UITextField!
    @IBOutlet weak var cityArrow: UIButton!
    @IBOutlet weak var cityTV: UITableView!
    @IBOutlet weak var areaView: UIView!
    @IBOutlet weak var areaTxtField: UITextField!
    @IBOutlet weak var areaArrow: UIButton!
    @IBOutlet weak var areaTV: UITableView!
    @IBOutlet weak var detailAddresTxtView: LocalizedTextView!
    @IBOutlet weak var phoneTxtField: CustomTxtField!
    @IBOutlet weak var detailsAddressView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var cityHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var areaHeightConstraint: NSLayoutConstraint!
    
    // MARK:- Instance Variables
    var cities = [City]()
    var areas = [Area]()
    var cityMenu = true
    var areaMenu = true
    var selectedCityId:Int?
    var selectedAreaId:Int?
    var user:User?
    var imagePicker:UIImagePickerController!
    var imgData:Data?
    let shapeLayer = CAShapeLayer()
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
        setupUserData()
    }
    
    override func viewDidLayoutSubviews() {
        self.topView.roundedFromSide(corners: [.bottomLeft, .bottomRight], cornerRadius: 12)
        let centre = CGPoint(x: userImgView.frame.width/2, y: userImgView.frame.height/2 + 15)
        let circularPath = UIBezierPath(arcCenter: centre, radius: 50, startAngle: 0, endAngle: CGFloat.pi, clockwise: true)
        shapeLayer.path = circularPath.cgPath
        shapeLayer.fillColor = UIColor.black.cgColor
        shapeLayer.opacity = 0.6
        userImg.layer.addSublayer(shapeLayer)
    }
    
    // MARK:- SetupUI
    func setupUserData(){
        guard let user = user, let city = user.city, let area = user.area, let img = user.img else{return}
        self.cityTxtField.text  = MOLHLanguage.isArabic() ?  city.arabicCityName : city.cityName
        self.areaTxtField.text  = MOLHLanguage.isArabic() ?  area.arabicAreaName : area.areaName
        self.selectedAreaId = area.id
        self.selectedCityId = city.id
        self.detailAddresTxtView.text = user.address
        self.firstnameTxtField.text = user.firstname
        self.lastnameTxtField.text = user.lastname
        self.emailTxtField.text = user.email
        self.phoneTxtField.text = user.phone
        self.userImg.setImage(imageUrl: img)
        loadCities()
        guard let cityID = city.id else{return}
        loadAreas(city: cityID)
    }
    
    func setupComponents(){
        topView.addShadowWith(color: UIColor.black, radius: 5, opacity: 0.6)
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .fullScreen
        firstnameTxtField.delegate = self
        lastnameTxtField.delegate = self
        emailTxtField.delegate = self
        cityTxtField.delegate = self
        areaTxtField.delegate = self
        phoneTxtField.delegate = self
        detailAddresTxtView.delegate = self
        cityTV.isHidden = true
        areaTV.isHidden = true
        userImgView.addCornerRadius(50)
        cityView.addBorderWith(width: 1.5, color:  UIColor.borderColor)
        cityView.addCornerRadius(8)
        areaView.addBorderWith(width: 1.5, color:  UIColor.borderColor)
        areaView.addCornerRadius(9)
        saveBtn.addBtnCornerRadius(8)
        saveBtn.addBtnShadowWith(color: UIColor.black, radius: 2, opacity: 0.2)
        detailsAddressView.addCornerRadius(8)
        detailsAddressView.addBorderWith(width: 1.5, color: UIColor.borderColor)
        detailsAddressView.addShadowWith(color: UIColor.shadowColor, radius: 3, opacity: 0.6)
        detailAddresTxtView.text = "Address".localized
    }
    
    // MARK:- DropDown Menuss
    @IBAction func buCityMenu(_ sender: Any) {
        self.loadCities()
        if cityMenu{
            UIView.animate(withDuration: 0.2, animations: {
                self.detailsAddressView.layer.borderColor = UIColor.borderColor.cgColor
                self.cityView.layer.borderColor = UIColor.selectedBorderColor.cgColor
                self.areaView.layer.borderColor = UIColor.borderColor.cgColor
                self.firstnameTxtField.layer.borderColor = UIColor.borderColor.cgColor
                self.lastnameTxtField.layer.borderColor = UIColor.borderColor.cgColor
                self.emailTxtField.layer.borderColor = UIColor.borderColor.cgColor
                self.phoneTxtField.layer.borderColor = UIColor.borderColor.cgColor
                self.cityTV.isHidden = false
                self.cityMenu = false
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.cityView.layer.borderColor = UIColor.borderColor.cgColor
                self.cityTV.isHidden = true
                self.cityMenu = true
            })
        }
    }
    
    func loadCities(){
        API.load_location(completion: { (err, cities: [City]?) in
            if err == nil{
                guard let cities = cities else{return}
                self.cityHeightConstraint.constant = CGFloat(cities.count * 40)
                self.cities = cities
                DispatchQueue.main.async {
                    self.cityTV.reloadData()
                }
            }else{
                self.showAlertError(title: "There is error may be connection lost".localized)
            }
        })
    }
    
    @IBAction func buAreaMenu(_ sender: Any) {
        if let cityID = self.selectedCityId{
            self.loadAreas(city: cityID)
            if areaMenu{
                UIView.animate(withDuration: 0.2, animations: {
                    self.detailAddresTxtView.layer.borderColor = UIColor.borderColor.cgColor
                    self.areaView.layer.borderColor = UIColor.selectedBorderColor.cgColor
                    self.cityView.layer.borderColor = UIColor.borderColor.cgColor
                    self.firstnameTxtField.layer.borderColor = UIColor.borderColor.cgColor
                    self.lastnameTxtField.layer.borderColor = UIColor.borderColor.cgColor
                    self.emailTxtField.layer.borderColor = UIColor.borderColor.cgColor
                    self.phoneTxtField.layer.borderColor = UIColor.borderColor.cgColor
                    self.areaTV.isHidden = false
                    self.areaMenu = false
                })
            }else{
                UIView.animate(withDuration: 0.2, animations: {
                    self.areaView.layer.borderColor = UIColor.borderColor.cgColor
                    self.areaTV.isHidden = true
                    self.areaMenu = true
                })
            }
            
        }else{
            self.showAlertWiring(title: "Please select city".localized)
        }
    }
    
    func loadAreas(city: Int){
        API.load_location(cityID: city, completion: { (err, areas: [Area]?) in
            if err == nil{
                guard let areas = areas else{return}
                self.areaHeightConstraint.constant = CGFloat(areas.count * 40)
                self.areas = areas
                DispatchQueue.main.async {
                    self.areaTV.reloadData()
                }
            }else{
                self.showAlertError(title: "There is error may be connection lost".localized)
            }
        })
    }
    
    // MARK:- Action
    @IBAction func editPhotoBtn(_ sender: Any) {
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            userImg.image = image
            imgData       = image.jpegData(compressionQuality: 0.75)
            print("Image Added Successfully")
        }
        imagePicker.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func SaveActionBtn(_ sender: Any) {
        if validData(){
            self.showLoadingInticator()
            guard let city = selectedCityId, let area = selectedAreaId, let address = detailAddresTxtView.text, let userID = NetworkHelper.getUserId()  else{return}
            let userData:Dictionary<String,Any> = [
                "firstname":    self.firstnameTxtField.text!,
                "lastname":      self.lastnameTxtField.text!,
                "email":            self.emailTxtField.text!,
                "phone":            self.phoneTxtField.text!,
                "type":                             "CLIENT",
                "city":                            "\(city)",
                "area":                            "\(area)",
                "address":                           address,
            ]
            let url = Constants.update_info + "\(userID)/updateInfo"
            API.sendRequest(userImage: imgData, method: .put, url: url, parameters: userData, header: Constants.header, completion: { (err,status,response: Response?) in
                self.hideLoadingInticator()
                if err == nil{
                    if status!{
                        guard let response = response else{return}
                        self.successUpdate(response: response)
                    }else{
                        self.showAlertWiring(title: "Update faild".localized)
                    }
                }else{
                    self.showAlertError(title: "There is error may be connection lost".localized)
                }
                
            })
        }
    }
    
    func successUpdate(response: Response){
        self.showAlertsuccess(title: "Update success".localized)
        if let user = response.user {
            guard let fname   = user.firstname, let lname   = user.lastname else{return}
            NetworkHelper.userEmail     = user.email
            NetworkHelper.userImage     = user.img
            NetworkHelper.userName      = fname+" "+lname
        }
        self.finish()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
