//
//  EditProfileVC+Extensions.swift
//  souqelteb
//
//  Created by HadyHammad on 12/21/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import MOLH

extension EditProfileVC{
    // MARK:- Data Validations
    func validData() ->Bool{
        
        if (firstnameTxtField.text!.isEmpty && lastnameTxtField.text!.isEmpty && emailTxtField.text!.isEmpty && cityTxtField.text!.isEmpty && phoneTxtField.text!.isEmpty && areaTxtField.text!.isEmpty && detailAddresTxtView.text!.isEmpty){
            self.showAlertWiring(title: "Please enter your data".localized)
            return false
        }
        
        if firstnameTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter first name".localized)
            return false
        }
        
        if firstnameTxtField.text!.count < 3{
            self.showAlertWiring(title: "Enter correct first name".localized)
            return false
        }
        
        if lastnameTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter last name".localized)
            return false
        }
        
        if lastnameTxtField.text!.count < 3{
            self.showAlertWiring(title: "Enter correct last name".localized)
            return false
        }
        
        if emailTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter email".localized)
            return false
        }
        
        if !(emailTxtField.text!.isValidEmail){
            self.showAlertWiring(title: "Enter valid email".localized)
            return false
        }
        
        if cityTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please select city".localized)
            return false
        }
       
        if areaTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please select area".localized)
            return false
        }
        
        if detailAddresTxtView.text!.isEmpty{
            self.showAlertWiring(title: "Please enter address".localized)
            return false
        }
        
        if phoneTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter phone".localized)
            return false
        }
        
        if phoneTxtField.text!.count != 11{
            self.showAlertWiring(title: "Phone number must be 11 digits".localized)
            return false
        }
        
        return true
    }
    
    func finish(){
        self.cityTxtField.text = ""
        self.areaTxtField.text = ""
        self.firstnameTxtField.text = ""
        self.lastnameTxtField.text = ""
        self.emailTxtField.text = ""
        self.phoneTxtField.text = ""
        self.detailAddresTxtView.text = ""
    }
  
}

extension EditProfileVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == areaTV{
            return areas.count
        }else{
            return cities.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == areaTV{
            if let cell = areaTV.dequeueReusableCell(withIdentifier: "areasCell"){
                let area = areas[indexPath.row]
                cell.textLabel?.text  = MOLHLanguage.isArabic() ?  area.arabicAreaName : area.areaName
                return cell
            }else{
                return UITableViewCell()
            }
        }else{
            if let cell = cityTV.dequeueReusableCell(withIdentifier: "citiesCell"){
                let city = cities[indexPath.row]
                cell.textLabel?.text  = MOLHLanguage.isArabic() ?  city.arabicCityName : city.cityName
                return cell
            }else{
                return UITableViewCell()
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == cityTV {
            self.selectedAreaId = nil
            self.areaTxtField.text = nil
            self.selectedCityId = self.cities[indexPath.row].id
            self.cityTxtField.text = MOLHLanguage.isArabic() ? self.cities[indexPath.row].arabicCityName : self.cities[indexPath.row].cityName
            
            UIView.animate(withDuration: 0.2, animations: {
                self.cityTV.isHidden = true
                self.cityMenu = true
            })
            
        }else{
            self.selectedAreaId = self.areas[indexPath.row].id
            self.areaTxtField.text = MOLHLanguage.isArabic() ? self.areas[indexPath.row].arabicAreaName : self.areas[indexPath.row].areaName
            
            UIView.animate(withDuration: 0.2, animations: {
                self.areaTV.isHidden = true
                self.areaMenu = true
            })
        }
    }
    
}

extension EditProfileVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == cityTxtField{
            cityView.layer.borderColor = UIColor.selectedBorderColor.cgColor
            areaView.layer.borderColor = UIColor.borderColor.cgColor
            self.areaTV.isHidden = true
        }else if textField == areaTxtField{
            areaView.layer.borderColor = UIColor.selectedBorderColor.cgColor
            cityView.layer.borderColor = UIColor.borderColor.cgColor
            self.cityTV.isHidden = true
        }else{
            textField.layer.borderColor = UIColor.selectedBorderColor.cgColor
            cityView.layer.borderColor = UIColor.borderColor.cgColor
            areaView.layer.borderColor = UIColor.borderColor.cgColor
            self.areaTV.isHidden = true
            self.cityTV.isHidden = true
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == cityTxtField{
            cityView.layer.borderColor = UIColor.borderColor.cgColor
        }else if textField == areaTxtField{
            areaView.layer.borderColor = UIColor.borderColor.cgColor
        }else{
            textField.layer.borderColor = UIColor.borderColor.cgColor
        }
        
    }
}

extension EditProfileVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
            detailsAddressView.layer.borderColor = UIColor.selectedBorderColor.cgColor
            cityView.layer.borderColor = UIColor.borderColor.cgColor
            areaView.layer.borderColor = UIColor.borderColor.cgColor
            self.areaTV.isHidden = true
            self.cityTV.isHidden = true
        }else{
            detailsAddressView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Address".localized
            textView.textColor = .lightGray
            detailsAddressView.layer.borderColor = UIColor.borderColor.cgColor
        }else{
            detailsAddressView.layer.borderColor = UIColor.borderColor.cgColor
        }
    }
}
