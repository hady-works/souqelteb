//
//  ProfileVC.swift
//  souqelteb
//
//  Created by Mac on 12/8/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class ProfileVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> ProfileVC{
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var usernameView: UIView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var chanePasswordView: UIView!
    @IBOutlet weak var topView: UIView!
    
    // MARK:- Instance Variables
    var user:User?
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadUserData()
    }
    
    override func viewDidLayoutSubviews() {
        self.topView.roundedFromSide(corners: [.bottomLeft, .bottomRight], cornerRadius: 12)
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        chanePasswordView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(changePassword)))
        topView.addShadowWith(color: UIColor.black, radius: 5, opacity: 0.6)
    }

    func loadUserData(){
        self.showLoadingInticator()
        guard let userID = NetworkHelper.getUserId() else{return}
        let url = Constants.baseURL + "\(userID)/getUser" 
        API.sendRequest(method: .get, url: url, completion: { (err,status,response: Response?) in
            self.hideLoadingInticator()
            if err == nil{
                if status!{
                    guard let user = response?.user else {return}
                    guard let fname = user.firstname else {return}
                    guard let lname = user.lastname else {return}
                    guard let img = user.img else {return}
                    self.user = user
                    //... Update UI
                    DispatchQueue.main.async {
                        self.usernameLbl.text = fname+" "+lname
                        self.emailLbl.text = user.email
                        self.phoneLbl.text = user.phone
                        self.locationLbl.text = user.address
                        self.userImg.setImage(imageUrl: img)
                        NetworkHelper.userImage = user.img
                    }
                }else{
                    self.showAlertWiring(title: "Can't load profile data".localized)
                }
            }else{
                self.showAlertError(title: "There is error may be connection lost".localized)
            }
        })
    }
    
    // MARK:- Action
    @objc func changePassword(){
        guard let user = self.user else{return}
        let changePasswordVC = ChangePasswordVC.instance()
        changePasswordVC.user = user
        self.present(changePasswordVC, animated: true, completion: nil)
    }
    
    @IBAction func buEditProfile(_ sender: Any) {
        guard let user = self.user else{return}
        let editProfileVC = EditProfileVC.instance()
        editProfileVC.user = user
        self.present(editProfileVC, animated: true, completion: nil)
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true){ () -> Void in
            self.present(MainTabBar.instance(), animated: true, completion: nil)
        }
        
    }
    
}
