//
//  SearchResultVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/24/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class SearchResultVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> SearchResultVC {
        let storyboard = UIStoryboard.init(name: "Product", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SearchResultVC") as! SearchResultVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var searchResultCV: UICollectionView!
    @IBOutlet weak var emptyStateLbl: UILabel!
    
    // MARK:- Instance Variables
    var resulrArray:[Product]?
    var productName:String?
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.view.backgroundColor = .white
        searchResultCV.registerCellNib(cellClass: ProductCVCell.self)
        getProducts()
    }
    
    func getProducts() {
        self.showLoadingInticator()
        guard let name = self.productName else{return}
        print(name)
        let url = Constants.search+"\(name)"
        let encoded = url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        API.sendRequest(method: .get, url: encoded ?? "", completion: { (err,status,response: ProductResponse?) in
            self.hideLoadingInticator()
            if err == nil{
                if status!{
                    guard let products = response?.data else{return}
                    if products.count == 0{
                        self.emptyStateLbl.isHidden = false
                    }
                    self.resulrArray = products
                    DispatchQueue.main.async {
                        self.searchResultCV.reloadData()
                    }
                }
            }else{
                self.emptyStateLbl.isHidden = false
            }
        })
    }
}

extension SearchResultVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let favouriteArray = self.resulrArray {
            return favouriteArray.count
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(indexPath: indexPath) as ProductCVCell
        if let resulrArray = self.resulrArray {
            cell.configure(product: resulrArray[indexPath.row])
            return cell
        }else{
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2 - 20, height: 220)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 12, bottom: 16, right: 12)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailsVC = ProductDetailsVC.instance()
        detailsVC.product = self.resulrArray?[indexPath.row]
        present(detailsVC, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
}
