//
//  ProductDetailsVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/11/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Cosmos
import MOLH

protocol ReviewDelegate {
    func reviewAdded()
}

class ProductDetailsVC: BaseViewController, ReviewDelegate {
    
    // MARK:- Instance
    static func instance () -> ProductDetailsVC {
        let storyboard = UIStoryboard.init(name: "Product", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var detailsCV: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var addToCartView: UIView!
    @IBOutlet weak var favouriteView: UIView!
    @IBOutlet weak var cartBtn: UIButton!
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var descTxtView: UITextView!
    @IBOutlet weak var descTxtViewHeight: NSLayoutConstraint!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var rateLbl: UILabel!
    @IBOutlet weak var stockCountLbl: UILabel!
    @IBOutlet weak var companyNameLbl: UILabel!
    @IBOutlet weak var companyView: UIView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var writeReviewBtn: UIButton!
    @IBOutlet weak var navTitleLbl: UILabel!
    @IBOutlet weak var cartImageView: UIImageView!
    @IBOutlet weak var colorTV: UITableView!
    @IBOutlet weak var sizeTV: UITableView!
    @IBOutlet weak var colorHeight: NSLayoutConstraint!
    @IBOutlet weak var sizeHeight: NSLayoutConstraint!
    @IBOutlet weak var colorBtn: UIButton!
    @IBOutlet weak var sizeBtn: UIButton!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var sizeView: UIView!
    @IBOutlet weak var colorLbl: UILabel!
    @IBOutlet weak var sizeLbl: UILabel!
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var badgeLbl: UILabel!
    @IBOutlet weak var colorStack: UIStackView!
    @IBOutlet weak var sizeStack: UIStackView!
    
    
    // MARK:- Instance variables
    var product:Product?
    var cartArray = NetworkHelper.getCart()
    var favouriteArray = NetworkHelper.getFavourite()
    var colorMenuShown = true
    var sizeMenuShown = true
    var selectedColor = ""
    var selectedSize = ""
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupComponents()
        setupUI()
    }
    
    override func viewDidLayoutSubviews() {
        guard let hasSize = product?.hasSizes,
            let hasColor  = product?.hasColors else{return}
        
        if hasColor {
            colorTV.roundedFromSide(corners:[.bottomLeft, .bottomRight], cornerRadius: 8)
        }
        
        if hasSize {
            sizeTV.roundedFromSide(corners:[.bottomLeft, .bottomRight], cornerRadius: 8)
        }
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        self.favouriteArray = NetworkHelper.getFavourite()
        self.cartArray = NetworkHelper.getCart()
        detailsCV.registerCellNib(cellClass: ProductDetailsCollectionViewCell.self)
        colorTV.registerCellNib(cellClass: DropDownTVC.self)
        sizeTV.registerCellNib(cellClass: DropDownTVC.self)
        sizeTV.addBorderWith(width: 1, color: .borderColor)
        colorTV.addBorderWith(width: 1, color: .borderColor)
        
        colorTV.isHidden = true
        sizeTV.isHidden = true
        badgeView.isHidden = true
        badgeView.addCornerRadius(badgeView.frame.size.height / 2)
        colorBtn.addBtnCornerRadius(5)
        sizeBtn.addBtnCornerRadius(5)
        colorView.addCornerRadius(5)
        sizeView.addCornerRadius(5)
        
        if let colorCount = product?.color?.count{
            colorHeight.constant = CGFloat(colorCount * 42)
        }
        
        if let sizeCount = product?.sizes?.count{
            sizeHeight.constant = CGFloat(sizeCount * 42)
        }
        
        if let count = self.product?.img?.count{
            pageControl.numberOfPages = count
        }
        
        if cartArray.count > 0 {
            badgeView.isHidden = false
            badgeLbl.text = "\(cartArray.count)"
        }
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            cartImageView.image = UIImage(named: "order_fliped")
        }else {
            cartImageView.image = UIImage(named: "OrderSelect")
        }
        
        addToCartView.addCornerRadius(5)
        favouriteView.addCornerRadius(5)
        addBtn.addBtnCornerRadius(5)
        addBtn.addBtnShadowWith(color: UIColor.black, radius: 2, opacity: 0.2)
        writeReviewBtn.addBtnCornerRadius(5)
        writeReviewBtn.addBtnShadowWith(color: UIColor.black, radius: 2, opacity: 0.2)
        companyView.addCornerRadius(5)
        companyView.addBorderWith(width: 1.5, color: UIColor.borderColor)
    }
    
    func setupUI(){
        if let product = self.product{
            guard let price         = product.price,
                let hasOffer      = product.hasOffer,
                let hasSize      = product.hasSizes,
                let hasColor      = product.hasColors,
                let offerPrice    = product.offerPrice,
                let ratePrecent   = product.ratePrecent,
                let rate          = product.rate,
                let quantity      = product.quantity,
                let available      = product.available,
                let id            = product.id else{return}
            
            navTitleLbl.text = product.name
            setupFavouriteAndCart(productID: id)
            descTxtView.text = product.description
            descTxtViewHeight.constant = self.descTxtView.contentSize.height + 8
            nameLbl.text = product.name
            rateLbl.text = "(\(rate))"
            
            if hasSize || hasColor {
                self.addBtn.alpha = 0.6
                self.addBtn.isUserInteractionEnabled = false
            }
            
            if !hasSize {
                sizeStack.isHidden = true
            }
            
            if !hasColor {
                colorStack.isHidden = true
            }
            
            if available {
                if MOLHLanguage.isArabic(){
                    self.stockCountLbl.text = "فقط \(quantity)" + " قطعة متبقية بالمتجر- أطلب قريباً";
                }else{
                    self.stockCountLbl.text = "Only \(quantity)" + " left in stock-order soon"
                }
            }else{
                self.stockCountLbl.text = "Not available in stock".localized
            }
            
            companyNameLbl.text = product.company
            self.rateView.rating = ratePrecent/20
            if hasOffer{
                priceLbl.text = "\(offerPrice)"
            }else{
                priceLbl.text = "\(price)"
            }
        }
    }
    
    func loadProduct() {
        guard let id = self.product?.id else{return}
        API.getProduct(id: id, completion: { (err, product) in
            if err == nil{
                self.product = product
                self.setupUI()
            }
            
        })
        API.sendRequest(method: .get, url: Constants.Products+"\(id)", completion: { (err, status, response: Product?) in
            if err == nil{
                if status!{
                    self.product = response
                    self.setupUI()
                }
            }
        })
    }
    
    func setupFavouriteAndCart(productID: Int){
        if (cartArray.contains(productID)){
            cartBtn.setImage(UIImage(named: "local_grocery_store"), for: .normal)
        }else{
            cartBtn.setImage(UIImage(named: "add_shopping_cart"), for: .normal)
        }
        
        if (favouriteArray.contains(productID)){
            favouriteBtn.setImage(UIImage(named: "favorite-1"), for: .normal)
        }else{
            favouriteBtn.setImage(UIImage(named: "favorite"), for: .normal)
        }
    }
    
    func reviewAdded() {
        loadProduct()
    }
    
    //MARK:- Buttons Actions
    @IBAction func buCart(_ sender: Any) {
        if let _ =  NetworkHelper.getAccessToken() {
            present(MyCartVC.instance(), animated: true, completion: nil)
        }else{
            self.present(LoginVC.instance(), animated: true, completion: nil)
        }
    }
    
    @IBAction func buTopAddToCart(_ sender: Any) {
        if let _ =  NetworkHelper.getAccessToken(), let productID = self.product?.id {
            if !(cartArray.contains(productID)){
                addToCart(id: productID)
            }else{
                removeFromCart(id: productID)
            }
        }else{
            self.present(LoginVC.instance(), animated: true, completion: nil)
        }
    }
    
    @IBAction func buFavourite(_ sender: Any) {
        
        if let _ =  NetworkHelper.getAccessToken(), let productID = self.product?.id {
            if !(favouriteArray.contains(productID)){
                addToFavourite(id: productID)
            }else{
                removeFromFavourite(id: productID)
            }
        }else{
            self.present(LoginVC.instance(), animated: true, completion: nil)
        }
    }
    
    func addToFavourite(id: Int){
        
        if let _ =  NetworkHelper.getAccessToken(){
            favouriteBtn.setImage(UIImage(named: "favorite-1"), for: .normal)
            API.sendRequest(method: .post, url: Constants.favourite+"\(id)/products", header: Constants.header, completion: { (err,status,response: Response?) in
                if err == nil{
                    if status!{
                        guard let arr = response?.user?.favourite else{return}
                        NetworkHelper.favourite = arr
                        self.favouriteArray = arr
                    }
                }
            })
        }else{
            self.present(LoginVC.instance(), animated: true, completion: nil)
        }
    }
    
    func addToCart(id: Int){
        
        if let _ =  NetworkHelper.getAccessToken(), let available = product?.available{
            if available{
                cartBtn.setImage(UIImage(named: "local_grocery_store"), for: .normal)
                guard let hasColor = product?.hasColors,
                    let hasSize = product?.hasSizes else{return}
                
                if hasColor && self.selectedColor == "" {
                    self.showAlertWiring(title: "Please select product color".localized)
                    return
                }
                
                if hasSize && self.selectedSize == "" {
                    self.showAlertWiring(title: "Please select product size".localized)
                    return
                }
                
                let parameter = ["color": self.selectedColor,
                                 "size" : self.selectedSize ]
                
                self.showAlertsuccess(title: "Product is added successfully".localized)
                self.badgeView.isHidden = false
                self.badgeLbl.text = "\(cartArray.count + 1)"
                
                API.sendRequest(method: .post, url: Constants.cart+"\(id)/products", parameters: parameter, header: Constants.header, completion: { (err,status,response: Response?) in
                    if err == nil{
                        if status!{
                            guard let arr = response?.user?.carts else{return}
                            NetworkHelper.cart = arr
                            self.cartArray = arr
                        }
                    }
                })
            }else{
                self.showAlertWiring(title: "Not available in stock".localized)
            }
        }else{
            self.present(LoginVC.instance(), animated: true, completion: nil)
        }
    }
    
    func removeFromFavourite(id: Int){
        
        if let _ =  NetworkHelper.getAccessToken(){
            favouriteBtn.setImage(UIImage(named: "favorite"), for: .normal)
            API.sendRequest(method: .delete, url: Constants.favourite+"\(id)", header: Constants.header, completion: { (err,status,response: Response?) in
                if err == nil{
                    if status!{
                        guard let arr = response?.user?.favourite else{return}
                        NetworkHelper.favourite = arr
                        self.favouriteArray = arr
                    }
                }
            })
        }else{
            self.present(LoginVC.instance(), animated: true, completion: nil)
        }
    }
    
    func removeFromCart(id: Int){
        
        if let _ =  NetworkHelper.getAccessToken(){
            cartBtn.setImage(UIImage(named: "add_shopping_cart"), for: .normal)
            if cartArray.count - 1 == 0{
                self.badgeView.isHidden = true
            }else{
                self.badgeLbl.text = "\(cartArray.count - 1)"
            }
            API.sendRequest(method: .delete, url: Constants.cart+"\(id)", header: Constants.header, completion: { (err,status,response: Response?) in
                if err == nil{
                    if status!{
                        guard let arr = response?.user?.carts else{return}
                        NetworkHelper.cart = arr
                        self.cartArray = arr
                    }
                }
            })
        }else{
            self.present(LoginVC.instance(), animated: true, completion: nil)
        }
    }
    
    @IBAction func buColorMenuPressed(_ sender: Any) {
        if colorMenuShown{
            UIView.animate(withDuration: 0.2, animations: {
                self.colorTV.isHidden = false
                self.colorMenuShown = false
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.colorTV.isHidden = true
                self.colorMenuShown = true
            })
        }
    }
    
    @IBAction func buSizeMenuPressed(_ sender: Any) {
        if sizeMenuShown{
            UIView.animate(withDuration: 0.2, animations: {
                self.sizeTV.isHidden = false
                self.sizeMenuShown = false
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.sizeTV.isHidden = true
                self.sizeMenuShown = true
            })
        }
    }
    
    @IBAction func buWriteReview(_ sender: Any) {
        let reviewVC = ReviewVC.instance()
        reviewVC.productID = self.product?.id
        reviewVC.delegate = self
        self.present(reviewVC, animated: true, completion: nil)
    }
    
    @IBAction func buAddToCart(_ sender: Any) {
        
        if let _ =  NetworkHelper.getAccessToken(), let productID = self.product?.id {
            if !(cartArray.contains(productID)){
                addToCart(id: productID)
            }else{
                self.showAlertWiring(title: "Product in cart already".localized)
            }
        }else{
            self.present(LoginVC.instance(), animated: true, completion: nil)
        }
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

// MARK:- CollectionView Extension
extension ProductDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let x = targetContentOffset.pointee.x
        let cellIndex = Int(x/view.frame.width)
        self.pageControl.currentPage = cellIndex
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let imagesArray = self.product?.img{
            return imagesArray.count
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(indexPath: indexPath) as ProductDetailsCollectionViewCell
        if let imagesArray = self.product?.img{
            cell.configure(imgURL: imagesArray[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

// MARK:- TableView Extension
extension ProductDetailsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == colorTV {
            return product?.color?.count ?? 0
        }else{
            return product?.sizes?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
            
        case colorTV:
            let cell = colorTV.dequeue() as DropDownTVC
            cell.cellLbl.text = product?.color?[indexPath.row] ?? ""
            return cell
            
        case sizeTV:
            let cell = sizeTV.dequeue() as DropDownTVC
            cell.cellLbl.text = product?.sizes?[indexPath.row] ?? ""
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 42
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == colorTV {
            UIView.animate(withDuration: 0.2, animations: {
                
                if let hasSize = self.product?.hasSizes, hasSize == true {
                    if self.selectedSize != "" {
                        self.addBtn.alpha = 1
                        self.addBtn.isUserInteractionEnabled = true
                    }else{
                        self.addBtn.alpha = 0.6
                        self.addBtn.isUserInteractionEnabled = true
                    }
                }else{
                    self.addBtn.alpha = 1
                    self.addBtn.isUserInteractionEnabled = true
                }
                
                let colorSelected = self.product?.color?[indexPath.row] ?? ""
                self.selectedColor = colorSelected
                self.colorLbl.text = colorSelected
                self.colorTV.isHidden = true
                self.colorMenuShown = true
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                
                if let hasColor = self.product?.hasColors, hasColor == true {
                    if self.selectedColor != "" {
                        self.addBtn.alpha = 1
                        self.addBtn.isUserInteractionEnabled = true
                    }else{
                        self.addBtn.alpha = 0.6
                        self.addBtn.isUserInteractionEnabled = true
                    }
                }else{
                    self.addBtn.alpha = 1
                    self.addBtn.isUserInteractionEnabled = true
                }
                
                let sizeSelected = self.product?.sizes?[indexPath.row] ?? ""
                self.selectedSize = sizeSelected
                self.sizeLbl.text = sizeSelected
                self.sizeTV.isHidden = true
                self.sizeMenuShown = true
            })
        }
    }
    
}
