//
//  RatingVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/25/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Cosmos
class RatingVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> RatingVC{
        let storyboard = UIStoryboard.init(name: "Product", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var commentTxtView: UITextView!
    @IBOutlet weak var commentView: UIView!
    
    // MARK:- Instance Variables
    var productID:Int?
    var delegate:ReviewDelegate!
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    func setupComponents(){
        commentTxtView.delegate = self
        sendBtn.addBtnCornerRadius(8)
        sendBtn.addBtnShadowWith(color: UIColor.black, radius: 2, opacity: 0.2)
        commentView.addCornerRadius(8)
        commentView.addBorderWith(width: 1.5, color: UIColor.borderColor)
        commentView.addShadowWith(color: UIColor.shadowColor, radius: 3, opacity: 0.6)
        commentTxtView.textColor = .lightGray
        commentTxtView.text = "Write comment".localized
    }
    
    // MARK:- Actions
    @IBAction func buSendReview(_ sender: Any) {
        let rate      = Int(ratingView.rating)
        let review:[String:Any] = [ "rate": rate,
                                    "comment": commentTxtView.text! ]
        
        guard let productID = self.productID else{return}
        let url = Constants.Products+"\(productID)/rate"
        self.showLoadingInticator()
        API.sendReview(url: url, reviewData: review, completion: { (err,status) in
            self.hideLoadingInticator()
            if (err == nil){
                if status!{
                    self.showAlertsuccess(title: "Sent success".localized)
                    self.delegate.reviewAdded()
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.showAlertWiring(title: "Check your data again".localized)
                }
            }else{
                self.showAlertError(title: "There is error may be connection lost".localized)
            }
        })
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension RatingVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
            commentView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }else{
            commentView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write comment".localized
            textView.textColor = .lightGray
            commentView.layer.borderColor = UIColor.borderColor.cgColor
        }else{
            commentView.layer.borderColor = UIColor.borderColor.cgColor
        }
    }
}
