//
//  ReviewVC.swift
//  souqelteb
//
//  Created by Hady Hammad on 2/12/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class ReviewVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> ReviewVC{
        let storyboard = UIStoryboard.init(name: "Product", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ReviewVC") as! ReviewVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var reviewsTV: UITableView!
    @IBOutlet weak var emptyStateLbl: UILabel!
    
    // MARK:- Instance variables
    var productID:Int?
    var totalRates:Int?
    var reviewsArray:[Review]?
    var delegate:ReviewDelegate?
    
    // MARK:- Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        reviewsTV.registerCellNib(cellClass: ReviewTVC.self)
        self.reviewsTV.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getReviews()
    }
    
    func getReviews() {
        self.showLoadingInticator()
        guard let productID = self.productID else{return}
        let url = Constants.Products+"\(productID)/findProductRate"
        API.sendRequest(method: .get, url: url, completion: { (err,status,response: ReviewResponse?) in
            self.hideLoadingInticator()
            if err == nil{
                if status!{
                    guard let reviews = response?.data else{return}
                    if reviews.count == 0{
                        self.emptyStateLbl.isHidden = false
                    }else{
                        self.reviewsTV.isHidden = false
                        self.emptyStateLbl.isHidden = true
                        self.reviewsArray = reviews
                        DispatchQueue.main.async {
                            self.reviewsTV.reloadData()
                        }
                    }
                }
            }else{
                self.emptyStateLbl.isHidden = false
            }
        })
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buWriteReview(_ sender: Any) {
        if let _ =  NetworkHelper.getAccessToken(){
            let ratingVC = RatingVC.instance()
            ratingVC.productID = self.productID
            ratingVC.delegate = delegate
            self.present(ratingVC, animated: true, completion: nil)
        }else{
            self.present(LoginVC.instance(), animated: true, completion: nil)
        }
    }
    
}

extension ReviewVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let reviewArray = self.reviewsArray{
            return reviewArray.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = reviewsTV.dequeue() as ReviewTVC
        let review = self.reviewsArray?[indexPath.row]
        cell.configure(review: review)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
}
