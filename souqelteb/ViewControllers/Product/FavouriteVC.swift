//
//  FavouriteVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/6/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
protocol RemoveFavouriteDelegate {
    func updateCollection(id: Int)
}
class FavouriteVC: BaseViewController, RemoveFavouriteDelegate {
    
    // MARK:- Instance
    static func instance () -> FavouriteVC{
        let storyboard = UIStoryboard.init(name: "Product", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "FavouriteVC") as! FavouriteVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var favouriteCV: UICollectionView!
    @IBOutlet weak var emptyStateLbl: UILabel!
    
    // MARK:- Instance Variables
    var favouriteArray:[Favourite]?
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        favouriteCV.registerCellNib(cellClass: ProductCVCell.self)
        getFavouriteProducts()
    }
    
    func updateCollection(id: Int) {
        self.favouriteArray?.removeAll{ $0.product?.id == id }
        self.favouriteCV.reloadData()
    }
    
    func getFavouriteProducts() {
        self.showLoadingInticator()
        guard let userID = NetworkHelper.getUserId() else{return}
        let url = Constants.favourite+"\(userID)/users"
        API.sendRequest(method: .get, url: url, completion: { (err,status,response: FavouriteResponse?) in
            self.hideLoadingInticator()
            if err == nil{
                if status!{
                    guard let favourites = response?.data else{return}
                    if favourites.count == 0{
                        self.emptyStateLbl.isHidden = false
                    }
                    self.favouriteArray = favourites
                    DispatchQueue.main.async {
                        self.favouriteCV.reloadData()
                    }
                }
            }else{
                self.emptyStateLbl.isHidden = false
            }
        })
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension FavouriteVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let favouriteArray = self.favouriteArray {
            return favouriteArray.count
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(indexPath: indexPath) as ProductCVCell
        if let favouriteArray = self.favouriteArray {
            cell.configure(product: favouriteArray[indexPath.row].product)
            cell.delegate = self
            cell.favouriteBtn.isUserInteractionEnabled = true
            return cell
        }else{
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2 - 20, height: 220)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 12, bottom: 16, right: 12)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
}
