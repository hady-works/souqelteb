//
//  CategoryProductVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/1/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import RangeSeekSlider
import MOLH

class CategoryProductVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> CategoryProductVC{
        let storyboard = UIStoryboard.init(name: "Product", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "CategoryProductVC") as! CategoryProductVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var categoryProductCV: UICollectionView!
    @IBOutlet weak var categoryNameLbl: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet var filterView: UIView!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var applyFilterBtn: UIButton!
    @IBOutlet weak var fromPriceTxtField: UITextField!
    @IBOutlet weak var toPriceTxtField: UITextField!
    @IBOutlet weak var hasOfferCheckBox: UIButton!
    @IBOutlet weak var highToLowCheckBox: UIButton!
    @IBOutlet weak var lowToHighCheckBox: UIButton!
    @IBOutlet weak var topRatedCheckBox: UIButton!
    @IBOutlet weak var priceSlider: RangeSeekSlider!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var emptyStateLbl: UILabel!
    @IBOutlet weak var searchText: UITextField!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchIconView: UIView!
    @IBOutlet weak var cartImageView: UIImageView!
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var badgeLbl: UILabel!
    
    // MARK:- Instance Variables
    var category:Category?
    var categoryProductsArray   = [Product]()
    var topRated                = false
    var hasOffer                = false
    var lowToHigh               = false
    var highToLow               = false
    var filterURL:String?
    var pageNumber = 1
    var pageCount : Int?
    var limit : Int?
    var searchArray:[Product]?
    var cartArray = NetworkHelper.getCart()
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
        categoryProductCV.registerCellNib(cellClass: ProductCVCell.self)
        getCategoryProducts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.cartArray = NetworkHelper.getCart()
        if cartArray.count > 0 {
            badgeView.isHidden = false
            badgeLbl.text = "\(cartArray.count)"
        }
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        self.topView.roundedFromSide(corners: [.bottomLeft, .bottomRight], cornerRadius: 10)
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        badgeView.isHidden = true
        badgeView.addCornerRadius(badgeView.frame.size.height / 2)
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            self.cartImageView.image = UIImage(named: "order_fliped")
        }else {
            self.cartImageView.image = UIImage(named: "OrderSelect")
        }
        guard let category = category, let categoryImage = category.img else{return}
        self.categoryNameLbl.text  = MOLHLanguage.isArabic() ?  category.arabicname : category.categoryname
        self.categoryImageView.setImage(imageUrl: categoryImage)
        toPriceTxtField.addBorderWith(width: 1.5, color: UIColor.borderColor)
        fromPriceTxtField.addBorderWith(width: 1.5, color: UIColor.borderColor)
        self.filterView.addCornerRadius(10)
        toPriceTxtField.addCornerRadius(3)
        fromPriceTxtField.addCornerRadius(3)
        applyFilterBtn.addCornerRadius(3)
        fromPriceTxtField.text = "\(Int(priceSlider.selectedMinValue))"
        toPriceTxtField.text = "\(Int(priceSlider.selectedMaxValue))"
        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(shadowTapped)))
        searchView.addCornerRadius(searchView.frame.height / 2.8)
        searchIconView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(search)))
        searchIconView.addCornerRadius(8)
        searchText.delegate = self
    }
    
    @objc func search(){
        if (searchText.text?.count)! != 0 {
            self.searchArray?.removeAll()
            for str in self.categoryProductsArray  {
                let range = str.name!.lowercased().range(of: searchText.text!, options: .caseInsensitive, range: nil, locale: nil)
                if range != nil{
                    self.searchArray?.append(str)
                }
                
            }
        }else{
            self.searchArray = self.categoryProductsArray
        }
        self.categoryProductCV.reloadData()
    }
    
    @objc func shadowTapped(){
        Hide_Filter_View()
    }
    
    @IBAction func buFilter(_ sender: Any) {
        Show_Filter_View()
    }
    
    @IBAction func buCart(_ sender: Any) {
        present(MyCartVC.instance(), animated: true, completion: nil)
    }
    
    @IBAction func buTopRatedCheckBox(_ sender: Any) {
        if !(topRated){
            topRatedCheckBox.setImage(UIImage(named: "check_box"), for: .normal)
            topRated = true
        }else{
            topRatedCheckBox.setImage(UIImage(named: "check_box-1"), for: .normal)
            topRated = false
        }
    }
    
    @IBAction func buHasOfferCheckBox(_ sender: Any) {
        if !(hasOffer){
            hasOfferCheckBox.setImage(UIImage(named: "check_box"), for: .normal)
            hasOffer = true
        }else{
            hasOfferCheckBox.setImage(UIImage(named: "check_box-1"), for: .normal)
            hasOffer = false
        }
    }
    
    @IBAction func buHighToLowCheckBox(_ sender: Any) {
        if !(highToLow){
            highToLowCheckBox.setImage(UIImage(named: "check_box"), for: .normal)
            highToLow = true
        }else{
            highToLowCheckBox.setImage(UIImage(named: "check_box-1"), for: .normal)
            highToLow = false
        }
    }
    
    @IBAction func buLowToHighCheckBox(_ sender: Any) {
        if !(lowToHigh){
            lowToHighCheckBox.setImage(UIImage(named: "check_box"), for: .normal)
            lowToHigh = true
        }else{
            lowToHighCheckBox.setImage(UIImage(named: "check_box-1"), for: .normal)
            lowToHigh = false
        }
    }
    
    @IBAction func buFilterApply(_ sender: Any) {
        if topRated{
            filterURL = filterURL!+"&top=\(true)"
        }
        
        if hasOffer{
            filterURL = filterURL!+"&hasOffer=\(true)"
        }
        
        if lowToHigh{
            filterURL = filterURL!+"&sortByPrice=\(true)"
        }
        
        if !(fromPriceTxtField.text!.isEmpty){
            filterURL = filterURL!+"&priceFrom=\(fromPriceTxtField.text!)"
        }
        
        if !(toPriceTxtField.text!.isEmpty){
            filterURL = filterURL!+"&priceTo=\(toPriceTxtField.text!)"
        }
        
        getFilterProducts(url: self.filterURL ?? "")
        
    }
    
    func Hide_Filter_View()
    {
        self.backView.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.filterView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.filterView.alpha = 0
            self.filterView.removeFromSuperview()
        }
    }
    
    func Show_Filter_View(){
        guard let categoryID = category?.id else{return}
        self.filterURL = Constants.category + "\(categoryID)"
        self.backView.alpha = 0.6
        self.view.addSubview(filterView)
        filterView.translatesAutoresizingMaskIntoConstraints = false
        filterView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        filterView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        filterView.heightAnchor.constraint(equalToConstant: 350).isActive = true
        filterView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.92).isActive = true
        filterView.transform = CGAffineTransform.init(scaleX:  1.3, y: 1.3)
        filterView.alpha = 0
        UIView.animate(withDuration: 0.5){
            self.filterView.alpha = 1
            self.filterView.transform = CGAffineTransform.identity
        }
    }
    
    func getFilterProducts(url: String) {
        self.showLoadingInticator()
        API.sendRequest(method: .get, url: url, completion: { (err,status,response: ProductResponse?) in
            self.hideLoadingInticator()
            if err == nil{
                if status!{
                    guard let products = response?.data else{return}
                    self.categoryProductsArray = products
                    self.searchArray = self.categoryProductsArray
                    if products.count == 0{
                        self.emptyStateLbl.isHidden = false
                    }else{
                        self.emptyStateLbl.isHidden = true
                    }
                    DispatchQueue.main.async {
                        self.Hide_Filter_View()
                        self.categoryProductCV.reloadData()
                    }
                }
            }else{
                self.Hide_Filter_View()
                self.emptyStateLbl.isHidden = false
            }
        })
    }
    
    func getCategoryProducts() {
        guard let id = category?.id else{return}
        let url = Constants.category + "\(id)&page=\(self.pageNumber)"
        self.showLoadingInticator()
        API.sendRequest(method: .get, url: url, completion: { (err,status,response: ProductResponse?) in
            self.hideLoadingInticator()
            if err == nil{
                if status!{
                    guard let products = response?.data else{return}
                    self.categoryProductsArray += products
                    self.searchArray = self.categoryProductsArray
                    self.pageCount = response?.pageCount
                    self.limit = response?.limit
                    if products.count == 0{
                        self.emptyStateLbl.isHidden = false
                    }
                    DispatchQueue.main.async {
                        self.categoryProductCV.reloadData()
                    }
                    self.Hide_Filter_View()
                }
            }else{
                self.Hide_Filter_View()
                self.emptyStateLbl.isHidden = false
            }
        })
    }
    
    @IBAction func changeValue(_ sender: Any) {
        let fromPrice = Int(self.priceSlider.selectedMinValue)
        let toPrice = Int(self.priceSlider.selectedMaxValue)
        self.fromPriceTxtField.text = "\(fromPrice)"
        self.toPriceTxtField.text = "\(toPrice)"
    }
    
}

extension CategoryProductVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.searchArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(indexPath: indexPath) as ProductCVCell
        if let product = self.searchArray?[indexPath.row] {
            cell.configure(product: product)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let limit = self.limit else{return}
        if indexPath.row == limit - 1 {
            guard let pageCount = self.pageCount else{return}
            if self.pageNumber < pageCount{
                self.pageNumber += 1
                self.getCategoryProducts()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2 - 20, height: 220)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 12, bottom: 16, right: 12)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailsVC = ProductDetailsVC.instance()
        detailsVC.product = self.searchArray?[indexPath.row]
        present(detailsVC, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
}

extension CategoryProductVC: UITextFieldDelegate {
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searchText.resignFirstResponder()
        searchText.text = ""
        self.searchArray?.removeAll()
        for str in self.categoryProductsArray {
            self.searchArray?.append(str)
        }
        self.categoryProductCV.reloadData()
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (searchText.text?.count)! != 0 {
            self.searchArray?.removeAll()
            for str in self.categoryProductsArray  {
                let range = str.name!.lowercased().range(of: textField.text!, options: .caseInsensitive, range: nil, locale: nil)
                if range != nil{
                    self.searchArray?.append(str)
                }
                
            }
        }else{
            self.searchArray = self.categoryProductsArray
        }
        self.categoryProductCV.reloadData()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            self.searchArray = self.categoryProductsArray
            self.categoryProductCV.reloadData()
        }
    }
    
}
