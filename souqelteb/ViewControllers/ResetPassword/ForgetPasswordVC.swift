//
//  ForgetPasswordVC.swift
//  souqelteb
//
//  Created by HadyHammad on 12/9/19.
//  Copyright © 2019 Mac. All rights reserved.
//
import UIKit
import MOLH

class ForgetPasswordVC: BaseViewController {
    
    static func instance () -> ForgetPasswordVC{
        let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ForgetPasswordVC") as! ForgetPasswordVC
    }
    
    @IBOutlet weak var emailTxtField: CustomTxtField!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var nextImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    override func viewDidLayoutSubviews() {
        self.topView.roundedFromSide(corners: [.bottomLeft, .bottomRight], cornerRadius: 25)
    }
    
    func setupComponents(){
        emailTxtField.delegate = self
        nextView.addCornerRadius(8)
        nextView.addShadowWith(color: UIColor.black, radius: 2, opacity: 0.2)
        nextView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(nextTapped)))
        topView.addShadowWith(color: UIColor.black, radius: 5, opacity: 0.6)
        if MOLHLanguage.isArabic(){
            self.nextImageView.image = UIImage(named: "arrow_back")
        }else{
            self.nextImageView.image = UIImage(named: "forward")
        }
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func nextTapped(){
        if validData(){
            self.showLoadingInticator()
            let parameters = [ "email": emailTxtField.text! ]
            API.reset_Password(url: Constants.forget_password, parameters: parameters, completion: { (err,msg) in
                self.hideLoadingInticator()
                if err == nil{
                    if msg == "Success"{
                        self.showAlertsuccess(title: "Verification code sent successfully".localized)
                        let verifyPasswordVC = VerifyPasswordVC.instance()
                        verifyPasswordVC.email = self.emailTxtField.text
                        self.present(verifyPasswordVC, animated: true, completion: nil)
                    }else{
                        guard let msg = msg else{return}
                        self.showAlertWiring(title: msg)
                    }
                }else{
                    self.showAlertError(title: "There is error may be connection lost".localized)
                }
            })
        }
    }
    
    func validData() -> Bool{
        
        if emailTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter email".localized)
            return false
        }
        
        if !(emailTxtField.text!.isValidEmail){
            self.showAlertWiring(title: "Enter valid email".localized)
            return false
        }
        
        return true
    }
    
}

extension ForgetPasswordVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.selectedBorderColor.cgColor
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.borderColor.cgColor
    }
}
