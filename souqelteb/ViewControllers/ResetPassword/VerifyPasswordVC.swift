//
//  VerifyPasswordVC.swift
//  souqelteb
//
//  Created by HadyHammad on 12/8/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import MOLH

class VerifyPasswordVC: BaseViewController {
    
    static func instance () -> VerifyPasswordVC {
        let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "VerifyPasswordVC") as! VerifyPasswordVC
    }
    
    @IBOutlet weak var digit1TxtField: UITextField!
    @IBOutlet weak var digit2TxtField: UITextField!
    @IBOutlet weak var digit3TxtField: UITextField!
    @IBOutlet weak var digit4TxtField: UITextField!
    
    @IBOutlet weak var digit1View: UIView!
    @IBOutlet weak var digit2View: UIView!
    @IBOutlet weak var digit3View: UIView!
    @IBOutlet weak var digit4View: UIView!
    
    @IBOutlet weak var userEmailTxtField: CustomTxtField!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var userEmailView: UIView!
    @IBOutlet weak var helpMessageLbl: UILabel!
    @IBOutlet weak var nextImageView: UIImageView!
    
    var email:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    override func viewDidLayoutSubviews() {
        self.topView.roundedFromSide(corners: [.bottomLeft, .bottomRight], cornerRadius: 25)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        digit1TxtField.becomeFirstResponder()
    }
    
    func setupComponents(){
        
        digit1TxtField.delegate = self
        digit2TxtField.delegate = self
        digit3TxtField.delegate = self
        digit4TxtField.delegate = self
        
        digit1TxtField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        digit2TxtField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        digit3TxtField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        digit4TxtField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        
        guard let email = email else{return}
        userEmailTxtField.text = email
        helpMessageLbl.text = "An email with verification code was just sent to ".localized + email
        userEmailView.addCornerRadius(15)
        nextView.addCornerRadius(8)
        nextView.addShadowWith(color: UIColor.black, radius: 2, opacity: 0.2)
        nextView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(nextTapped)))
        
        topView.addShadowWith(color: UIColor.black, radius: 5, opacity: 0.6)
        digit1View.addCornerRadius(8)
        digit1View.addShadowWith(color: UIColor.shadowColor, radius: 3, opacity: 0.4)
        digit1View.addBorderWith(width: 1.5, color: UIColor.borderColor)
        
        digit2View.addCornerRadius(8)
        digit2View.addShadowWith(color: UIColor.shadowColor, radius: 5, opacity: 0.7)
        digit2View.addBorderWith(width: 1.5, color: UIColor.borderColor)
        
        digit3View.addCornerRadius(8)
        digit3View.addShadowWith(color: UIColor.shadowColor, radius: 5, opacity: 0.7)
        digit3View.addBorderWith(width: 1.5, color: UIColor.borderColor)
        
        digit4View.addCornerRadius(8)
        digit4View.addShadowWith(color: UIColor.shadowColor, radius: 5, opacity: 0.7)
        digit4View.addBorderWith(width: 1.5, color: UIColor.borderColor)
        
        if MOLHLanguage.isArabic(){
            self.nextImageView.image = UIImage(named: "arrow_back")
        }else{
            self.nextImageView.image = UIImage(named: "forward")
        }
        
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func nextTapped(){
        if validCode(){
            self.showLoadingInticator()
            guard let email = email else{return}
            let verifyCode = "\(digit1TxtField.text!)\(digit2TxtField.text!)\(digit3TxtField.text!)\(digit4TxtField.text!)"
            
            let parameters = [ "email": email, "verifycode": verifyCode]
            API.reset_Password(url: Constants.confirm_Code, parameters: parameters, completion: { (err,msg) in
                self.hideLoadingInticator()
                if err == nil{
                    if msg == "Success"{
                        self.showAlertsuccess(title: "Success code".localized)
                        let updatePasswordVC = UpdatePasswordVC.instance()
                        updatePasswordVC.email = self.email
                        self.present(updatePasswordVC, animated: true, completion: nil)
                    }else{
                        self.showAlertWiring(title: msg!)
                    }
                }else{
                    self.showAlertError(title: "There is error may be connection lost".localized)
                }
            })
        }
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        let text = textField.text
        if text?.utf16.count == 1{
            switch textField{
            case digit1TxtField:
                digit2TxtField.becomeFirstResponder()
            case digit2TxtField:
                digit3TxtField.becomeFirstResponder()
            case digit3TxtField:
                digit4TxtField.becomeFirstResponder()
            case digit4TxtField:
                digit4TxtField.becomeFirstResponder()
            default:
                break
            }
        }else{
            print("ERT...")
        }
    }
    
    func validCode() -> Bool{
        if digit1TxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please fill all digits".localized)
            return false
        }
        
        if digit2TxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please fill all digits".localized)
            return false
        }
        
        if digit3TxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please fill all digits".localized)
            return false
        }
        
        if digit4TxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please fill all digits".localized)
            return false
        }
        
        return true
    }
    
}


extension VerifyPasswordVC: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        switch textField{
            
            
        case digit1TxtField:
            digit1View.layer.borderColor = UIColor.verifyBorderColor
        case digit2TxtField:
            digit2View.layer.borderColor = UIColor.verifyBorderColor
        case digit3TxtField:
            digit3View.layer.borderColor = UIColor.verifyBorderColor
        case digit4TxtField:
            digit4View.layer.borderColor = UIColor.verifyBorderColor
            
        default:
            print("Any")
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField{
            
        case digit1TxtField:
            if digit1TxtField.text == ""{
                digit1View.layer.borderColor = UIColor.borderColor.cgColor
            }
        case digit2TxtField:
            if digit2TxtField.text == ""{
                digit2View.layer.borderColor =  UIColor.borderColor.cgColor
            }
        case digit3TxtField:
            if digit3TxtField.text == ""{
                digit3View.layer.borderColor =  UIColor.borderColor.cgColor
            }
        case digit4TxtField:
            if digit4TxtField.text == ""{
                digit4View.layer.borderColor =  UIColor.borderColor.cgColor
            }
            
        default:
            print("Any")
        }
    }
}
