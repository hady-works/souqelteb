//
//  UpdatePasswordVC.swift
//  souqelteb
//
//  Created by HadyHammad on 12/8/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import MOLH

class UpdatePasswordVC: BaseViewController {
    
    static func instance () -> UpdatePasswordVC {
        let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "UpdatePasswordVC") as! UpdatePasswordVC
    }
    
    @IBOutlet weak var newPasswordTxtField: CustomTxtField!
    @IBOutlet weak var confirmPasswordTxtField: CustomTxtField!
    @IBOutlet weak var newPasswordVisibility: UIButton!
    @IBOutlet weak var confirmPasswordVisibility: UIButton!
    @IBOutlet weak var userEmailTxtField: CustomTxtField!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var userEmailView: UIView!
    @IBOutlet weak var nextImageView: UIImageView!
    
    var passwordSecure = true
    var confirmSecure = true
    var email:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    override func viewDidLayoutSubviews() {
        self.topView.roundedFromSide(corners: [.bottomLeft, .bottomRight], cornerRadius: 25)
    }
    
    func setupComponents(){
        newPasswordTxtField.delegate = self
        confirmPasswordTxtField.delegate = self
        guard let email = email else{return}
        userEmailTxtField.text = email
        userEmailView.addCornerRadius(15)
        newPasswordView.addBorderWith(width: 1.5, color:  UIColor.borderColor)
        newPasswordView.addCornerRadius(8)
        confirmPasswordView.addBorderWith(width: 1.5, color:  UIColor.borderColor)
        confirmPasswordView.addCornerRadius(8)
        nextView.addCornerRadius(8)
        nextView.addShadowWith(color: UIColor.black, radius: 2, opacity: 0.2)
        nextView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(nextTapped)))
        topView.addShadowWith(color: UIColor.black, radius: 5, opacity: 0.6)
        if MOLHLanguage.isArabic(){
            self.nextImageView.image = UIImage(named: "arrow_back")
        }else{
            self.nextImageView.image = UIImage(named: "forward")
        }
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func nextTapped(){
        if validData(){
            self.showLoadingInticator()
            guard let email = email else{return}
            let parameters = [ "email": email, "newPassword": confirmPasswordTxtField.text!]
            
            API.reset_Password( url: Constants.reset_password, parameters: parameters, completion: { (err,msg) in
                self.hideLoadingInticator()
                if err == nil{
                    if msg == "Success"{
                        self.showAlertsuccess(title: "Reset password success".localized)
                        self.finish()
                        self.present(LoginVC.instance(), animated: true, completion: nil)
                    }else{
                        self.showAlertWiring(title: "Reset password faild".localized)
                    }
                }else{
                    self.showAlertError(title: "There is error may be connection lost".localized)
                }
            })
        }
    }
    
    func validData() -> Bool{
        
        if newPasswordTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter new password".localized)
            return false
        }
        
        if newPasswordTxtField.text!.count <= 5{
            self.showAlertWiring(title: "Password is less than 6".localized)
            return false
        }
        
        if confirmPasswordTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter confirm password".localized)
            return false
        }
        
        if confirmPasswordTxtField.text!.count <= 5{
            self.showAlertWiring(title: "Password is less than 6".localized)
            return false
        }
        
        if newPasswordTxtField.text != confirmPasswordTxtField.text{
            self.showAlertWiring(title: "Confirm password not match".localized)
            return false
        }
        
        return true
    }
    
    func finish(){
        self.newPasswordTxtField.text = ""
        self.confirmPasswordTxtField.text = ""
    }
    
    @IBAction func showPassword(_ sender: Any) {
        
        if passwordSecure{
            switchSecureEntry(secure: passwordSecure, passwordField: newPasswordTxtField, visibility: newPasswordVisibility)
            passwordSecure = false
        }else{
            switchSecureEntry(secure: passwordSecure, passwordField: newPasswordTxtField, visibility: newPasswordVisibility)
            passwordSecure = true
        }
        
    }
    
    @IBAction func showConfirmedPassword(_ sender: Any) {
        
        if confirmSecure{
            switchSecureEntry(secure: confirmSecure, passwordField: confirmPasswordTxtField, visibility: confirmPasswordVisibility)
            confirmSecure = false
        }else{
            switchSecureEntry(secure: confirmSecure, passwordField: confirmPasswordTxtField, visibility: confirmPasswordVisibility)
            confirmSecure = true
        }
        
    }
    
}

extension UpdatePasswordVC: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == newPasswordTxtField{
            newPasswordView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }
        
        if textField == confirmPasswordTxtField{
            confirmPasswordView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == newPasswordTxtField{
            newPasswordView.layer.borderColor = UIColor.borderColor.cgColor
        }
        
        if textField == confirmPasswordTxtField{
            confirmPasswordView.layer.borderColor = UIColor.borderColor.cgColor
        }
    }
    
}
