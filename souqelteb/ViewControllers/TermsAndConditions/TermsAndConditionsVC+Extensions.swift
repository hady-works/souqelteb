//
//  TermsAndConditionsVC+Extensions.swift
//  souqelteb
//
//  Created by Mac on 12/14/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit

extension TermsAndConditionsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.TermsAndConditions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = tableView.dequeue() as TermsAndConditionsTVCell
         let model = self.TermsAndConditions[indexPath.row]
         cell.configure(model: model)
        return cell
    }
    
}
