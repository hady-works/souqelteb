//
//  TermsAndConditionsVC.swift
//  souqelteb
//
//  Created by Mac on 12/13/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class TermsModel {
    
    var title: String?
    var details: String?
    
    init(title: String, details: String) {
        self.title = title
        self.details = details
        
    }
    
}

class TermsAndConditionsVC: UIViewController {
    
    static func instance () -> TermsAndConditionsVC{
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "TermsAndConditionsVC") as! TermsAndConditionsVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var dataTV: UITableView!
    @IBOutlet weak var policyView: UIView!
    
    // MARK:- Properties
    let viewModel = TermsAndConditionsViewModel()
    var TermsAndConditions = [TermsModel]()
    var fromLogin:Bool?
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if let fromLogin = fromLogin, fromLogin == true{
            self.policyView.isHidden = false
        }else{
            self.policyView.isHidden = true
        }
        dataTV.delegate = self
        dataTV.dataSource = self
        dataTV.registerCellNib(cellClass: TermsAndConditionsTVCell.self)
        self.viewModel.getTermsAndConditionsData()
        BindViewModel()
        
    }
    
    override func viewDidLayoutSubviews() {
        self.policyView.roundedFromSide(corners: [.topLeft, .topRight], cornerRadius: 20)
    }
    
    func BindViewModel(){
        
        viewModel.termsAndConditionseData.bind { (data) in
            if let data = data {
                DispatchQueue.main.async {
                    self.TermsAndConditions.append(TermsModel(title: "About", details: data.about ?? ""))
                    self.TermsAndConditions.append(TermsModel(title: "Usage", details: data.usage ?? ""))
                    self.TermsAndConditions.append(TermsModel(title: "Conditions", details: data.conditions ?? ""))
                    self.TermsAndConditions.append(TermsModel(title: "Privacy", details: data.privacy ?? ""))
                    self.dataTV.reloadData()
                }
            }
        }
        
        viewModel.errorMsg.bind { (msg) in
            if let msg = msg {
                AlertUtility.showAlert(title: "Error".localized, message: msg, VC: self)
            }
            
        }
    }
    
    @IBAction func buPrivacyPolicy(_ sender: Any) {
        if let url = URL(string: "https://Souqelteeb.000webhostapp.com"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


