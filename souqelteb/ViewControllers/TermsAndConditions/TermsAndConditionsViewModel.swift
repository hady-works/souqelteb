//
//  TermsAndConditionsViewModel.swift
//  souqelteb
//
//  Created by Mac on 12/14/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation

class TermsAndConditionsViewModel {
    
    // MARK:- Properties
    var termsAndConditionseData = Observer<TermsAndConditionsModel>()
    var errorMsg = Observer<String>()
    
    
    // MARK:- Calling Api in viewModel
    internal func getTermsAndConditionsData(){
        TermsAndConditionsApi.shared.getMyJusoorProfileData { (isDone, msg, resualt) in
            if isDone {
                guard let value = resualt else { return }
                self.termsAndConditionseData.value = value
            }else {
                //Show Error
                self.errorMsg.value = msg
            }
        }
    }
    
}
