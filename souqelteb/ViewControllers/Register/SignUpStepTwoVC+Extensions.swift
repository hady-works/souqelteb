//
//  SignUpStepTwoVC+Extensions.swift
//  souqelteb
//
//  Created by HadyHammad on 12/21/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import MOLH

extension SignUpStepTwoVC{
    // MARK:- Data Validations
    func validData() ->Bool{
        
        if cityTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please select city".localized)
            return false
        }
    
        if areaTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please select area".localized)
            return false
        }
        
        if addressTxtView.text!.isEmpty{
            self.showAlertWiring(title: "Please enter address".localized)
            return false
        }
        
        return true
    }
    
    func finish(){
        self.cityTxtField.text = ""
        self.areaTxtField.text = ""
        self.addressTxtView.text = ""
    }
}

extension SignUpStepTwoVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == areasTable{
            return areas.count
        }else{
            return cities.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == areasTable{
            if let cell = areasTable.dequeueReusableCell(withIdentifier: "areasCell"){
                let area = areas[indexPath.row]
                cell.textLabel?.text = MOLHLanguage.isArabic() ?  area.arabicAreaName : area.areaName
                return cell
            }else{
                return UITableViewCell()
            }
        }else{
            if let cell = citiesTable.dequeueReusableCell(withIdentifier: "citiesCell"){
                let city = cities[indexPath.row]
                cell.textLabel?.text = MOLHLanguage.isArabic() ?  city.arabicCityName : city.cityName
                return cell
            }else{
                return UITableViewCell()
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == citiesTable{
            self.selectedArea = nil
            self.areaTxtField.text = nil
            self.selectedCity = self.cities[indexPath.row].id
            self.cityTxtField.text = MOLHLanguage.isArabic() ? self.cities[indexPath.row].arabicCityName : self.cities[indexPath.row].cityName
            
            UIView.animate(withDuration: 0.2, animations: {
                self.citiesTable.isHidden = true
                self.cityMenu = true
            })
            
        }else{
            self.selectedArea = self.areas[indexPath.row].id
            self.areaTxtField.text = MOLHLanguage.isArabic() ? self.areas[indexPath.row].arabicAreaName : self.areas[indexPath.row].areaName
            
            UIView.animate(withDuration: 0.2, animations: {
                self.areasTable.isHidden = true
                self.areaMenu = true
            })
        }
    }
    
}

extension SignUpStepTwoVC: UITextFieldDelegate,UITextViewDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == cityTxtField{
            cityView.layer.borderColor = UIColor.selectedBorderColor.cgColor
            areaView.layer.borderColor = UIColor.borderColor.cgColor
            self.areasTable.isHidden = true
        }else if textField == areaTxtField{
            areaView.layer.borderColor = UIColor.selectedBorderColor.cgColor
            cityView.layer.borderColor = UIColor.borderColor.cgColor
            self.citiesTable.isHidden = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == cityTxtField{
            cityView.layer.borderColor = UIColor.borderColor.cgColor
        }else if textField == areaTxtField{
            areaView.layer.borderColor = UIColor.borderColor.cgColor
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
            addressView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }else{
            addressView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Address".localized
            textView.textColor = .lightGray
            addressView.layer.borderColor = UIColor.borderColor.cgColor
        }else{
            addressView.layer.borderColor = UIColor.borderColor.cgColor
        }
    }
    
}
