//
//  SignUpStepTwoVC.swift
//  souqelteb
//
//  Created by HadyHammad on 12/8/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import Alamofire

class SignUpStepTwoVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> SignUpStepTwoVC{
        let storyboard = UIStoryboard.init(name: "Signup", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SignUpStepTwoVC") as! SignUpStepTwoVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var cityTxtField: UITextField!
    @IBOutlet weak var areaTxtField: UITextField!
    @IBOutlet weak var addressTxtView: UITextView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var citiesTable: UITableView!
    @IBOutlet weak var areasTable: UITableView!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var areaView: UIView!
    @IBOutlet weak var cityHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var areaHeightConstraint: NSLayoutConstraint!
    
    // MARK:- Instance Variables
    var cities = [City]()
    var areas = [Area]()
    var cityMenu = true
    var areaMenu = true
    var selectedCity:Int?
    var selectedArea:Int?
    var userData:NSMutableDictionary?
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
        DispatchQueue.main.async {
            self.loadCities()
        }
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        cityTxtField.delegate = self
        areaTxtField.delegate = self
        addressTxtView.delegate = self
        citiesTable.isHidden = true
        areasTable.isHidden = true
        cityView.addBorderWith(width: 1.5, color:  UIColor.borderColor)
        cityView.addCornerRadius(8)
        areaView.addBorderWith(width: 1.5, color:  UIColor.borderColor)
        areaView.addCornerRadius(8)
        signUpBtn.addBtnCornerRadius(8)
        signUpBtn.addBtnShadowWith(color: UIColor.black, radius: 2, opacity: 0.2)
        loginBtn.underline()
        addressView.addCornerRadius(8)
        addressView.addBorderWith(width: 1.5, color: UIColor.borderColor)
        addressView.addShadowWith(color: UIColor.shadowColor, radius: 3, opacity: 0.6)
        addressTxtView.textColor = .lightGray
        addressTxtView.text = "Address".localized
    }
    
    // MARK:- DropDown Menus
    func loadCities(){
        API.load_location(completion: { (err, cities: [City]?) in
            if err == nil{
                guard let cities = cities else{return}
                self.cityHeightConstraint.constant = CGFloat(cities.count * 40)
                self.cities = cities
                DispatchQueue.main.async {
                    self.citiesTable.reloadData()
                }
            }else{
                self.showAlertError(title: "There is error may be connection lost".localized)
            }
        })
    }
    
    @IBAction func buCityMenu(_ sender: Any) {
        if cityMenu{
            UIView.animate(withDuration: 0.2, animations: {
                self.addressView.layer.borderColor = UIColor.borderColor.cgColor
                self.cityView.layer.borderColor = UIColor.selectedBorderColor.cgColor
                self.areaView.layer.borderColor = UIColor.borderColor.cgColor
                self.citiesTable.isHidden = false
                self.cityMenu = false
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.cityView.layer.borderColor = UIColor.borderColor.cgColor
                self.citiesTable.isHidden = true
                self.cityMenu = true
            })
        }
    }
    
    @IBAction func buAreaMenu(_ sender: Any) {
        if let city = self.selectedCity{
            API.load_location(cityID: city, completion: { (err, areas: [Area]?) in
                if err == nil{
                    guard let areas = areas else{return}
                    self.areaHeightConstraint.constant = CGFloat(areas.count * 40)
                    self.areas = areas
                    DispatchQueue.main.async {
                        self.areasTable.reloadData()
                    }
                }else{
                    self.showAlertError(title: "There is error may be connection lost".localized)
                }
            })
            
            if areaMenu{
                UIView.animate(withDuration: 0.2, animations: {
                    self.addressView.layer.borderColor = UIColor.borderColor.cgColor
                    self.areaView.layer.borderColor = UIColor.selectedBorderColor.cgColor
                    self.cityView.layer.borderColor = UIColor.borderColor.cgColor
                    self.areasTable.isHidden = false
                    self.areaMenu = false
                })
            }else{
                UIView.animate(withDuration: 0.2, animations: {
                    self.areaView.layer.borderColor = UIColor.borderColor.cgColor
                    self.areasTable.isHidden = true
                    self.areaMenu = true
                })
            }
            
        }else{
            self.showAlertWiring(title: "Please select city".localized)
        }
    }
    
    // MARK:- Action
    @IBAction func buRegister(_ sender: Any) {
        
        if validData(){
            guard let city = selectedCity else{return}
            guard let area = selectedArea else{return}
            guard let address = addressTxtView.text else{return}
            
            self.userData?.setValue("\(city)", forKey: "city")
            self.userData?.setValue("\(area)", forKey: "area")
            self.userData?.setValue(address, forKey: "address")
            
            guard let parameter = self.userData as? [String : Any] else{return}
            self.showLoadingInticator()
            API.sendRequest(method: .post, url: Constants.sign_up, parameters: parameter, completion: { (err,status,response: LoginResponse?) in
                self.hideLoadingInticator()
                if err == nil{
                    if status!{
                        guard let response = response else{return}
                        self.successRegister(response: response)
                    }else{
                        self.showAlertWiring(title: "Sign up faild".localized)
                    }
                }else{
                    self.showAlertError(title: "There is error may be connection lost".localized)
                }
            })
        }
        
    }
    
    func successRegister(response: LoginResponse){
        self.showAlertsuccess(title: "Sign up success".localized)
        if let user = response.user {
            guard let fname   = user.firstname else{return}
            guard let lname   = user.lastname else{return}
            NetworkHelper.userID        = user.id
            NetworkHelper.accessToken   = response.token!
            NetworkHelper.userEmail     = user.email
            NetworkHelper.favourite     = user.favourite ?? []
            NetworkHelper.cart          = user.carts ?? []
            NetworkHelper.userImage     = user.img
            NetworkHelper.city          = user.city?.id
            NetworkHelper.area          = user.area?.id
            NetworkHelper.userName      = fname+" "+lname
            Constants.isLogIn           = true
        }
        self.finish()
        self.present(MainTabBar.instance(), animated: true, completion: nil)
    }
    
    @IBAction func buLogin(_ sender: Any) {
        self.present(LoginVC.instance(), animated: true, completion: nil)
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
