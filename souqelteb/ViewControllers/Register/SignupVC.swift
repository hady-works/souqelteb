//
//  SignupVC.swift
//  souqelteb
//
//  Created by Mac on 12/5/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import Messages
import Firebase

class SignupVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> SignupVC{
        let storyboard = UIStoryboard.init(name: "Signup", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var fnameTxtField: CustomTxtField!
    @IBOutlet weak var lnameTxtField: CustomTxtField!
    @IBOutlet weak var emailTxtField: CustomTxtField!
    @IBOutlet weak var passwordTxtField: CustomTxtField!
    @IBOutlet weak var confirmPasswordTxtField: CustomTxtField!
    @IBOutlet weak var phoneTxtField: CustomTxtField!
    @IBOutlet weak var confirmVisibility: UIButton!
    @IBOutlet weak var passwordVisibility: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var acceptBtn: UIButton!
    
    // MARK:- Variables
    var passwordSecure = true
    var confirmSecure = true
    var accept = false
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        
        fnameTxtField.delegate = self
        lnameTxtField.delegate = self
        emailTxtField.delegate = self
        passwordTxtField.delegate = self
        confirmPasswordTxtField.delegate = self
        phoneTxtField.delegate = self
        
        passwordView.addBorderWith(width: 1.5, color: UIColor.borderColor)
        passwordView.addCornerRadius(8)
        
        confirmPasswordView.addBorderWith(width: 1.5, color: UIColor.borderColor)
        confirmPasswordView.addCornerRadius(8)
        
        nextBtn.addBtnCornerRadius(8)
        nextBtn.addBtnShadowWith(color: UIColor.black, radius: 2, opacity: 0.2)
        
        loginBtn.underline()
        
    }
    
    // MARK:- Actions
    @IBAction func buLogin(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buNext(_ sender: Any) {
        if validData(){
            emailAndPhoneCheck(email: emailTxtField.text!, phone: phoneTxtField.text!)
        }
    }
    
    @IBAction func buAcceptPolicy(_ sender: Any) {
        if !accept{
            acceptBtn.setImage(UIImage(named: "check_box"), for: .normal)
            accept = true
        }else{
            acceptBtn.setImage(UIImage(named: "check_box-1"), for: .normal)
            accept = false
        }
    }
    
    @IBAction func buTermsAndConditions(_ sender: Any) {
        let termsAndConditionsVC = TermsAndConditionsVC.instance()
        termsAndConditionsVC.fromLogin = true
        present(termsAndConditionsVC, animated: true, completion: nil)
    }
    
    func emailAndPhoneCheck(email: String, phone: String){
        let parameters = ["email": self.emailTxtField.text!]
        self.showLoadingInticator()
        API.checkDataExist(parameters: parameters, url: Constants.check_email, completion: { (err, status) in
            self.hideLoadingInticator()
            if err == nil{
                if status == false{
                    self.phoneCheck(phone: phone)
                }else{
                    self.showAlertWiring(title: "Email already exist".localized)
                }
            }else{
                self.showAlertError(title: "There is error may be connection lost".localized)
            }
        })
        
    }
    
    func phoneCheck(phone: String){
        guard let fcmToken = Messaging.messaging().fcmToken else { return }
        let parameters = ["phone": self.phoneTxtField.text!]
        API.checkDataExist(parameters: parameters, url: Constants.check_phone, completion: { (err, status) in
            if err == nil{
                if status == false{
                    
                    var userData = NSMutableDictionary.init()
                    userData = [
                        "firstname":    self.fnameTxtField.text!,
                        "lastname":     self.lnameTxtField.text!,
                        "email":        self.emailTxtField.text!,
                        "phone":        self.phoneTxtField.text!,
                        "type":                 "CLIENT",
                        "signUpFrom":           "Normal",
                        "img":                        "",
                        "token":                      fcmToken,
                        "password":     self.passwordTxtField.text!,
                    ]
                    
                    self.showAlertsuccess(title: "Step1 completed successfully".localized)
                    let signUpStepTwoVC = SignUpStepTwoVC.instance()
                    signUpStepTwoVC.userData = userData
                    self.present(signUpStepTwoVC, animated: true, completion: nil)
                }else{
                    self.showAlertWiring(title: "Phone already exist".localized)
                }
                
            }else{
                self.showAlertError(title: "There is error may be connection lost".localized)
            }
        })
    }
    
    @IBAction func buShowPassword(_ sender: Any) {
        if passwordSecure{
            switchSecureEntry(secure: passwordSecure, passwordField: passwordTxtField, visibility: passwordVisibility)
            passwordSecure = false
        }else{
            switchSecureEntry(secure: passwordSecure, passwordField: passwordTxtField, visibility: passwordVisibility)
            passwordSecure = true
        }
    }
    
    @IBAction func buShowConfirmPassword(_ sender: Any) {
        if confirmSecure{
            switchSecureEntry(secure: confirmSecure, passwordField: confirmPasswordTxtField, visibility: confirmVisibility)
            confirmSecure = false
        }else{
            switchSecureEntry(secure: confirmSecure, passwordField: confirmPasswordTxtField, visibility: confirmVisibility)
            confirmSecure = true
        }
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- Data Validations
    func validData() -> Bool{
        
        if (fnameTxtField.text!.isEmpty&&lnameTxtField.text!.isEmpty&&emailTxtField.text!.isEmpty&&passwordTxtField.text!.isEmpty&&confirmPasswordTxtField.text!.isEmpty&&phoneTxtField.text!.isEmpty){
            self.showAlertWiring(title: "Please enter your data".localized)
            return false
        }
        
        if fnameTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter first name".localized)
            return false
        }
        
        if fnameTxtField.text!.count < 3{
            self.showAlertWiring(title: "Please enter correct first name".localized)
            return false
        }
        
        if lnameTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter last name".localized)
            return false
        }
        
        if lnameTxtField.text!.count < 3{
            self.showAlertWiring(title: "Please enter correct last name".localized)
            return false
        }
        
        if emailTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter email".localized)
            return false
        }
        
        if !(emailTxtField.text!.isValidEmail){
            self.showAlertWiring(title: "Enter valid email".localized)
            return false
        }
        
        if passwordTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter password".localized)
            return false
        }
        
        if passwordTxtField.text!.count <= 5{
            self.showAlertWiring(title: "Password is less than 6".localized)
            return false
        }
        
        if confirmPasswordTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter confirm password".localized)
            return false
        }
        
        if passwordTxtField.text != confirmPasswordTxtField.text{
            self.showAlertWiring(title: "Confirm password not match".localized)
            return false
        }
        
        if phoneTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter phone".localized)
            return false
        }
        
        if phoneTxtField.text!.count != 11{
            self.showAlertWiring(title: "Phone number must be 11 digits".localized)
            return false
        }
        
        if !accept{
            self.showAlertWiring(title: "You must agree this terms".localized)
            return false
        }
        
        return true
    }
}


extension SignupVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == passwordTxtField{
            passwordView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }else if textField == confirmPasswordTxtField{
            confirmPasswordView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }else{
            textField.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == passwordTxtField{
            passwordView.layer.borderColor = UIColor.borderColor.cgColor
        }else if textField == confirmPasswordTxtField{
            confirmPasswordView.layer.borderColor = UIColor.borderColor.cgColor
        }else{
            textField.layer.borderColor = UIColor.borderColor.cgColor
        }
    }
}
