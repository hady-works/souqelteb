//
//  OrdersVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/20/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class OrdersVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> OrdersVC{
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "OrdersVC") as! OrdersVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var ordersMainTV: UITableView!
    @IBOutlet weak var emptyStateLbl: UILabel!
    
    // MARK:- Instance Variables
    var ordersArray = [Order]()
    var pageNumber = 1
    var pageCount : Int?
    var limit : Int?
    
    
    // MARK:- Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if let _ =  NetworkHelper.getAccessToken() {
            self.navigationItem.title = "Orders".localized
            ordersMainTV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
            loadOrders()
        }else{
            self.present(LoginVC.instance(), animated: true, completion: nil)
        }
    }
    
    func loadOrders() {
        guard let userID = NetworkHelper.getUserId() else{return}
        let url = Constants.getOrders+"\(userID)&page=\(self.pageNumber)"
        self.showLoadingInticator()
        API.sendRequest(method: .get, url: url, completion: { (err,status,response: OrdersResponse?) in
            self.hideLoadingInticator()
            if err == nil{
                if status!{
                    guard let ordersArray = response?.data else{return}
                    self.pageCount = response?.pageCount
                    self.limit = response?.limit
                    if ordersArray.count == 0{
                        self.emptyStateLbl.isHidden = false
                    }
                    self.ordersArray += ordersArray
                    self.ordersMainTV.reloadData()
                }
            }else{
                self.emptyStateLbl.isHidden = false
            }
        })
    }
    
}

extension OrdersVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ordersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ordersMainTV.dequeueReusableCell(withIdentifier: "OrderMainTVC", for: indexPath) as! OrderMainTVC
        cell.configure(order: self.ordersArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let limit = self.limit else{return}
        if indexPath.row == limit - 1 {
            guard let pageCount = self.pageCount else{return}
            if self.pageNumber < pageCount{
                self.pageNumber += 1
                self.loadOrders()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let productCount = self.ordersArray[indexPath.row].productOrders?.count {
            return CGFloat(productCount * 150) + 170
        }else{
            return UITableView.automaticDimension
        }
    }
    
}
