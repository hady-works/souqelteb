//
//  HomeVC.swift
//  souqelteb
//
//  Created by HadyHammad on 12/30/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import MOLH
import SideMenu

protocol ProductDetailDelegate {
    func goToDetails(product: Product)
}

class HomeVC: BaseViewController, ProductDetailDelegate, UISearchBarDelegate {
    
    //Mark:- Outlets
    @IBOutlet weak var productsTV: UITableView!
    @IBOutlet weak var emptyStateLbl: UILabel!
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    
    //Mark:- Instance Variables
    var sections = [String]()
    var offersArray         = [Product]()
    var topSellerArray      = [Product]()
    var firstCategoryArray  = [Product]()
    var secondCategoryArray = [Product]()
    var advertiseArray      = [Advertise]()
    var arr : [[Product]] = []
    var cartArray = NetworkHelper.getCart()
    
    
    //Mark:- Lifecycel
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addNotificationBt(navigationItem: self.navigationItem)
        setupComponents()
        createSearchBar()
    }
    
    //... Add Search Bar
    func createSearchBar(){
        let searchBar = UISearchBar()
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.backgroundColor = UIColor.white
        }
        searchBar.contentMode = MOLHLanguage.isArabic() ? .right : .left
        searchBar.semanticContentAttribute = MOLHLanguage.isArabic() ? .forceRightToLeft : .forceLeftToRight
        
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            if let textView = textfield.subviews.first{
                textView.backgroundColor = .white
                textView.layer.cornerRadius = 17
                textView.clipsToBounds = true
            }
            
            textfield.textAlignment = MOLHLanguage.isArabic() ? .right : .left
            textfield.placeholder = "What are you looking for?".localized
            textfield.font = UIFont(name: "Roboto-regular", size: self.view.frame.height * 0.022)
            textfield.clipsToBounds = true
            textfield.textColor = UIColor(red: 55/255, green: 97/255, blue: 116/255, alpha: 1)
        }
        searchBar.showsCancelButton = false
        searchBar.delegate = self
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchVC = SearchResultVC.instance()
        searchVC.productName = searchBar.text
        navigationController?.pushViewController(searchVC, animated: true)
    }
    
    //... Add Navigation bar with badge
    private func addNotificationBt(navigationItem:UINavigationItem)
    {
        self.cartArray = NetworkHelper.getCart()
        let notificationBt = UIButton.init(frame: CGRect.init(x: 0, y: 10, width: 20, height: 20))
        notificationBt.addTarget(self, action: #selector(moveToCart(_:)), for: .touchUpInside)
        var notificationCountLb = UILabel.init(frame: CGRect.init(x: 18, y: -8, width: 20, height: 20))
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            notificationCountLb = UILabel.init(frame: CGRect.init(x: -18, y: -8, width: 20, height: 20))
            notificationBt.setImage(UIImage(named: "order_fliped"), for: .normal)
            self.menuBtn.image = UIImage(named: "sort_fliped")
        }else {
            notificationCountLb = UILabel.init(frame: CGRect.init(x: 18, y: -8, width: 20, height: 20))
            notificationBt.setImage(UIImage(named: "OrderSelect"), for: .normal)
            self.menuBtn.image = UIImage(named: "sort")
        }
        
        notificationCountLb.backgroundColor = UIColor.red
        notificationCountLb.clipsToBounds = true
        notificationCountLb.layer.cornerRadius = notificationCountLb.bounds.height / 2
        notificationCountLb.textColor = UIColor.white
        notificationCountLb.font = UIFont(name: "Roboto-Bold", size: 15)
        notificationCountLb.textAlignment = .center
        
        if cartArray.count > 0 {
            notificationCountLb.isHidden = false
            notificationCountLb.text = "\(cartArray.count)"
        }else{
            notificationCountLb.isHidden = true
        }
        notificationBt.addSubview(notificationCountLb)
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: notificationBt)
    }
    
    @objc func moveToCart(_ sender:Any)
    {
        if let _ =  NetworkHelper.getAccessToken() {
            present(MyCartVC.instance(), animated: true, completion: nil)
        }else{
            self.present(LoginVC.instance(), animated: true, completion: nil)
        }
    }
    
    @IBAction func buMenu(_ sender: Any) {
        if MOLHLanguage.currentAppleLanguage() == "ar"{
            if let sideManager = SideMenuManager.default.menuRightNavigationController{
                present(sideManager, animated: true, completion: nil)
            }
        }else {
            if let sideManager = SideMenuManager.default.menuLeftNavigationController{
                present(sideManager, animated: true, completion: nil)
            }
        }
        
    }
    
    // MARK:- SetupUI
    func setupComponents() {
        self.showLoadingInticator()
        self.productsTV.isHidden = true
        self.sections = [ "Ads","Offers", "Top Sellers"]
        getAdvertise()
        getOffers()
        getTopSeller()
        getCategories()
    }
    
    func goToDetails(product: Product) {
        let detailsVC = ProductDetailsVC.instance()
        detailsVC.product = product
        self.present(detailsVC, animated: true, completion: nil)
    }
    
}

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = productsTV.dequeueReusableCell(withIdentifier: "AdsCell", for: indexPath) as! AdvertisementTVC
            cell.pageControl.numberOfPages = self.advertiseArray.count
            cell.advertisementCV.registerCellNib(cellClass: AdvertisementCVC.self)
            cell.advertiseArray = self.advertiseArray
            cell.advertisementCV.reloadData()
            return cell
        }else{
            let cell = productsTV.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeCell
            setupTableCell(cell: cell, indexPath: indexPath)
            return cell
        }
        
    }
    
    func setupTableCell(cell: HomeCell, indexPath: IndexPath) {
        switch indexPath.row {
        case 1:
            cell.pageControl.numberOfPages = self.offersArray.count
            cell.emptyLbl.text = "No Offers Yet".localized
            cell.emptyLbl.isHidden = self.offersArray.isEmpty ? false : true
            cell.titleView.isHidden = false
            cell.pageControl.isHidden = false
        case 2:
            cell.pageControl.numberOfPages = self.topSellerArray.count
            cell.emptyLbl.isHidden = self.topSellerArray.isEmpty ? false : true
            cell.titleView.isHidden = false
            cell.pageControl.isHidden = false
        case 3:
            cell.pageControl.numberOfPages = self.firstCategoryArray.count
            cell.titleView.isHidden = false
            cell.pageControl.isHidden = false
        case 4:
            cell.pageControl.numberOfPages = self.secondCategoryArray.count
            cell.titleView.isHidden = false
            cell.pageControl.isHidden = false
        default:
            cell.pageControl.numberOfPages = 3
            cell.titleView.isHidden = false
            cell.pageControl.isHidden = false
        }
        cell.productCollectionView.registerCellNib(cellClass: ProductCVCell.self)
        cell.firstCategoryArray = self.firstCategoryArray
        cell.secondCategoryArray = self.secondCategoryArray
        cell.offersArray = self.offersArray
        cell.topSellerArray = self.topSellerArray
        cell.sectionTitle.text = sections[indexPath.row].localized
        cell.productCollectionView.tag = indexPath.row
        cell.delegate = self
        cell.productCollectionView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return self.view.bounds.height * 0.36
        }else{
            return 300
        }
    }
    
}

