//
//  HomeVC+Extensions.swift
//  souqelteb
//
//  Created by HadyHammad on 1/1/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import MOLH

extension HomeVC{
    func getTopSeller(){
        API.sendRequest(method: .get, url: Constants.topSeller, completion: { (err,status,response: ProductResponse?) in
            if err == nil{
                if status!{
                    guard let topSellers = response?.data else{return}
                    self.topSellerArray = topSellers
                    self.emptyStateLbl.isHidden = true
                    self.productsTV.isHidden = false
                    DispatchQueue.main.async {
                        self.productsTV.reloadData()
                    }
                }
            }else{
                self.hideLoadingInticator()
                self.emptyStateLbl.isHidden = false
            }
        })
    }
    
    func getOffers(){
        API.sendRequest(method: .get, url: Constants.offers, completion: { (err,status,response: ProductResponse?) in
            if err == nil{
                if status!{
                    guard let hasOffers = response?.data else{return}
                    self.emptyStateLbl.isHidden = true
                    self.productsTV.isHidden = false
                    self.offersArray = hasOffers
                    DispatchQueue.main.async {
                        self.productsTV.reloadData()
                    }
                }
            }else{
                self.hideLoadingInticator()
                self.emptyStateLbl.isHidden = false
            }
        })
    }
    
    func getAdvertise(){
        API.sendRequest(method: .get, url: Constants.ads, completion: { (err,status,response: AdevrtiseResponse?) in
            if err == nil{
                if status!{
                    guard let advertiseArray = response?.data else{return}
                    self.emptyStateLbl.isHidden = true
                    self.advertiseArray = advertiseArray
                    DispatchQueue.main.async {
                        self.productsTV.reloadData()
                    }
                }
            }else{
                self.hideLoadingInticator()
                self.emptyStateLbl.isHidden = false
            }
        })
    }
    
    func getFirstTwoCategory(completeion: @escaping (_ categories:[Category]?)->()){
        API.sendRequest(method: .get, url: Constants.categories, completion: { (err,status,response: CategoryResponse?) in
            if err == nil{
                guard let categories = response?.data else{return}
                DispatchQueue.main.async {
                    if categories.count > 1{
                        self.productsTV.isHidden = false
                        self.emptyStateLbl.isHidden = true
                        let array = [ categories[categories.count - 2], categories[categories.count - 1] ]
                        completeion(array)
                    }else{
                        if self.topSellerArray.isEmpty && self.offersArray.isEmpty && self.advertiseArray.isEmpty{
                            self.emptyStateLbl.isHidden = false
                        }
                    }
                }
            }else{
                self.hideLoadingInticator()
                if self.topSellerArray.isEmpty && self.offersArray.isEmpty && self.advertiseArray.isEmpty{
                    self.emptyStateLbl.isHidden = false
                }
            }
        })
    }
    
    func getCategories(){
        getFirstTwoCategory(completeion: { array in
            if let firstCategory = array?[1]{
                self.getFirstCategory(category: firstCategory)
            }
            if let secondCategory = array?[0]{
                self.getSecondCategory(category: secondCategory)
            }
        })
    }
    
    func getFirstCategory(category: Category?){
        guard let categoryName = category?.categoryname, let categoryArabicName = category?.arabicname, let categoryID = category?.id else{return}
        API.sendRequest(method: .get, url: Constants.category+"\(categoryID)", completion: { (err,status,response: ProductResponse?) in
            if err == nil{
                if status!{
                    guard let firstCategory = response?.data else{return}
                    self.emptyStateLbl.isHidden = true
                    if !(firstCategory.isEmpty){
                        self.firstCategoryArray = firstCategory
                        self.sections.append(MOLHLanguage.isArabic() ?  categoryArabicName : categoryName)
                    }
                    DispatchQueue.main.async {
                        self.productsTV.reloadData()
                    }
                }
            }else{
                self.hideLoadingInticator()
            }
        })
    }
    
    func getSecondCategory(category: Category?){
        guard let categoryName = category?.categoryname, let categoryArabicName = category?.arabicname, let categoryID = category?.id else{return}
        API.sendRequest(method: .get, url: Constants.category+"\(categoryID)", completion: { (err,status,response: ProductResponse?) in
            self.hideLoadingInticator()
            if err == nil{
                if status!{
                    guard let secondCategory = response?.data else{return}
                    self.emptyStateLbl.isHidden = true
                    if !(secondCategory.isEmpty){
                        self.secondCategoryArray = secondCategory
                        self.sections.append(MOLHLanguage.isArabic() ?  categoryArabicName : categoryName)
                    }
                    DispatchQueue.main.async {
                        self.productsTV.reloadData()
                    }
                }
            }else{
                self.hideLoadingInticator()
            }
        })
    }
    
}
