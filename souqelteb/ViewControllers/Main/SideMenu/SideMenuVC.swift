//
//  SideMenuVC.swift
//  souqelteb
//
//  Created by HadyHammad on 12/22/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import MOLH

class SideMenuVC: BaseViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var categoriesBtn: UIButton!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var languageBtn: UIButton!
    @IBOutlet weak var contactUsBtn: UIButton!
    @IBOutlet weak var termsAndConditionsBtn: UIButton!
    @IBOutlet weak var usagePolicyBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var logoutStack: UIStackView!
    
    static var delegate: IndexChangeDelegate? = nil
    var use:User?
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponent()
    }
    
    func setupComponent() {
        let origImage = UIImage(named: "catahorySelect")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        categoryImageView.image = tintedImage
        categoryImageView.tintColor = UIColor(red: 34/255, green: 37/255, blue: 53/255, alpha: 1)
        if MOLHLanguage.currentAppleLanguage() == "ar"{
            homeBtn.setTitle("الصفحة الرئيسية", for: .normal)
            categoriesBtn.setTitle("الأقسام", for: .normal)
            profileBtn.setTitle("الملف الشخصي", for: .normal)
            favoriteBtn.setTitle("المفضلة", for: .normal)
            languageBtn.setTitle("اللغة", for: .normal)
            contactUsBtn.setTitle("تواصل معنا", for: .normal)
            termsAndConditionsBtn.setTitle("الأحكام والشروط", for: .normal)
            usagePolicyBtn.setTitle("سياسة الإستخدام", for: .normal)
            shareBtn.setTitle("شارك التطبيق", for: .normal)
            logoutBtn.setTitle("تسجيل الخروج", for: .normal)
        }
    }
    
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        if let _ =  NetworkHelper.getAccessToken() {
            self.logoutStack.isHidden = false
        }else{
            self.logoutStack.isHidden = true
        }
        // Inject Side Menu By Logged User Data
        userName.text  = NetworkHelper.getUserName()
        userEmail.text = NetworkHelper.getUserEmail()
        guard let image = NetworkHelper.getuserImage() else{return}
        userImage.setImage(imageUrl: image)
    }
    
    // MARK: - Helper's
    func changeLanguage(){
        
        let alert = UIAlertController(title: "Choose Language".localized, message: "", preferredStyle: .actionSheet)
        let actionEnglish = UIAlertAction(title: "\("English")", style: .default) { (_) in
            if MOLHLanguage.currentAppleLanguage() == "ar"{
                MOLHLanguage.setDefaultLanguage("en")
                MOLH.setLanguageTo("en")
                self.setCollectionAppearance()
                self.dismiss(animated: true, completion: {
                    MOLH.reset()
                })
            }
        }
        let actionArabic = UIAlertAction(title: "\("العربية")", style: .default) { (_) in
            
            if MOLHLanguage.currentAppleLanguage() == "en"{
                MOLHLanguage.setDefaultLanguage("ar")
                MOLH.setLanguageTo("ar")
                self.setCollectionAppearance()
                self.dismiss(animated: true, completion: {
                    MOLH.reset()
                })
            }
        }
        let actionCancel = UIAlertAction(title: "Cancel".localized, style: .cancel) { (_) in
        }
        alert.addAction(actionEnglish)
        alert.addAction(actionArabic)
        alert.addAction(actionCancel)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func setCollectionAppearance() {
        UICollectionView.appearance().semanticContentAttribute = MOLHLanguage.isRTLLanguage() ? .forceRightToLeft : .forceLeftToRight
    }
    
    
    // MARK:- Action
    @IBAction func buHome(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            SideMenuVC.delegate?.changeTabBarIndex(index: 0)
        })
    }
    
    @IBAction func buProfile(_ sender: Any) {
        if let _ =  NetworkHelper.getAccessToken() {
            self.present(ProfileVC.instance(), animated: true, completion: nil)
        }else{
            self.dismiss(animated: true, completion: {
                UIApplication.shared.keyWindow?.rootViewController = LoginVC.instance()
            })
        }
    }
    
    @IBAction func buFavorite(_ sender: Any) {
        if let _ =  NetworkHelper.getAccessToken() {
            self.present(FavouriteVC.instance(), animated: true, completion: nil)
        }else{
            self.dismiss(animated: true, completion: {
                UIApplication.shared.keyWindow?.rootViewController = LoginVC.instance()
            })
        }
    }
    
    @IBAction func buCategories(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            SideMenuVC.delegate?.changeTabBarIndex(index: 3)
        })
    }
    
    @IBAction func buLanguage(_ sender: Any) {
        self.changeLanguage()
    }
    
    @IBAction func buContactUs(_ sender: Any) {
        present(ContactUsVC.instance(), animated: true, completion: nil)
    }
    
    @IBAction func buTermsAndConditions(_ sender: Any) {
        present(TermsAndConditionsVC.instance(), animated: true, completion: nil)
    }
    
    @IBAction func buUsagePolicy(_ sender: Any) {
        if let url = URL(string: "https://Souqelteeb.000webhostapp.com"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func buShareApp(_ sender: Any) {
        
        if let myWebsite = NSURL(string: "") {
            let objectsToShare = ["", myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.popoverPresentationController?.sourceView = self.view
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func buLogOut(_ sender: Any) {
        NetworkHelper.logoutUserIdCleanUp()
        NetworkHelper.logoutUserEmailCleanUp()
        NetworkHelper.logoutFavouriteCleanUp()
        NetworkHelper.logoutCartCleanUp()
        NetworkHelper.logoutUserNameCleanUp()
        NetworkHelper.logoutAccessTokenCleanUp()
        NetworkHelper.logoutAreaCleanUp()
        NetworkHelper.logoutCityCleanUp()
        Constants.isLogIn = false
        self.dismiss(animated: true){ () -> Void in
            UIApplication.shared.keyWindow?.rootViewController = LoginVC.instance()
        }
    }
    
}
