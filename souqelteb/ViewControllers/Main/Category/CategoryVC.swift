//
//  CategoryVC.swift
//  souqelteb
//
//  Created by HadyHammad on 12/27/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class CategoryVC: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // MARK:- Outlets
    @IBOutlet weak var categoryTV: UICollectionView!
    @IBOutlet weak var emptyStateLbl: UILabel!
    
    // MARK:- Instance Variables
    var categoriesArray = [Category]()
    
    // MARK:- Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Categories".localized
        categoryTV.registerCellNib(cellClass: CategoryCollectionViewCell.self)
        loadCategories()
    }
    
    func loadCategories(){
        self.showLoadingInticator()
        API.sendRequest(method: .get, url: Constants.categories, completion: { (err,status,response: CategoryResponse?) in
            self.hideLoadingInticator()
            if err == nil{
                guard let categories = response?.data else{return}
                if categories.count == 0{
                    self.emptyStateLbl.isHidden = false
                }
                self.categoriesArray = categories
                DispatchQueue.main.async {
                    self.categoryTV.reloadData()
                }
            }else{
                self.emptyStateLbl.isHidden = false
            }
        })
    }
    
    // MARK:- CollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categoriesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(indexPath: indexPath) as CategoryCollectionViewCell
        cell.configure(category: self.categoriesArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/3 - 10, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let categoryProductVC = CategoryProductVC.instance()
        categoryProductVC.category = self.categoriesArray[indexPath.row]
        self.navigationController?.pushViewController(categoryProductVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
}
