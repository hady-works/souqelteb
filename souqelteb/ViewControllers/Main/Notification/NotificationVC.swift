//
//  NotificationVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/26/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class NotificationVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> NotificationVC{
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var notificationTV: UITableView!
    @IBOutlet weak var emptyStateLbl: UILabel!
    
    // MARK:- Instance Variables
    var notificationArray = [Notification]()
    var pageNumber = 1
    var pageCount : Int?
    var limit : Int?
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let _ =  NetworkHelper.getAccessToken() {
            self.navigationItem.title = "Notifications".localized
            notificationTV.registerCellNib(cellClass: NotificationTVC.self)
            notificationTV.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
            notificationTV.isHidden = true
            getNotifications()
        }else{
            self.present(LoginVC.instance(), animated: true, completion: nil)
        }
    }
    
    func getNotifications(){
        self.showLoadingInticator()
        API.sendRequest(method: .get, url: Constants.getNotification + "\(self.pageNumber)", header: Constants.header, completion: { (err,status,response: NotificationResponse?) in
            self.hideLoadingInticator()
            if err == nil{
                if status!{
                    guard let notifications = response?.data else{return}
                    self.pageCount = response?.pageCount
                    self.limit = response?.limit
                    if notifications.count == 0{
                        self.emptyStateLbl.isHidden = false
                    }
                    self.notificationArray += notifications
                    self.notificationTV.isHidden = false
                    DispatchQueue.main.async {
                        self.notificationTV.reloadData()
                    }
                }
            }else{
                self.emptyStateLbl.isHidden = false
            }
        })
    }
}

extension NotificationVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = notificationTV.dequeue() as NotificationTVC
        cell.configure(notify: self.notificationArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let limit = self.limit else{return}
        if indexPath.row == limit - 1 {
            guard let pageCount = self.pageCount else{return}
            if self.pageNumber < pageCount{
                self.pageNumber += 1
                self.getNotifications()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        var actionsArray:[UIContextualAction] = [deleteAction(at: indexPath)]
        if let read = self.notificationArray[indexPath.row].read {
            if read{
                actionsArray.append(unReadAction(at: indexPath))
            }else{
                actionsArray.append(readAction(at: indexPath))
            }
        }
        return UISwipeActionsConfiguration(actions: actionsArray)
    }
    
    func readAction(at indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "read", handler: { (action,view,completion) in
            guard let id = self.notificationArray[indexPath.row].id else{return}
            let url = Constants.Notification + "/\(id)/read"
            API.mark_Notification(url: url, method: .put, completion: { status in
                if status!{
                    self.notificationArray.removeAll()
                    self.getNotifications()
                }
            })
            completion(true)
        })
        action.backgroundColor = UIColor(red: 192/255, green: 192/255, blue: 192/255, alpha: 1)
        return action
    }
    
    func unReadAction(at indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "Unread", handler: { (action,view,completion) in
            
            guard let id = self.notificationArray[indexPath.row].id else{return}
            let url = Constants.Notification + "/\(id)/unread"
            API.mark_Notification(url: url, method: .put, completion: { status in
                if status!{
                    self.notificationArray.removeAll()
                    self.getNotifications()
                }
            })
            completion(true)
        })
        action.backgroundColor = UIColor(red: 192/255, green: 192/255, blue: 192/255, alpha: 1)
        return action
    }
    
    func deleteAction(at indexPath: IndexPath) -> UIContextualAction{
        let action = UIContextualAction(style: .normal, title: "Delete", handler: { (action,view,completion) in
            guard let id = self.notificationArray[indexPath.row].id else{return}
            let url = Constants.Notification + "/\(id)/delete"
            API.mark_Notification(url: url, method: .delete, completion: { status in
                if status!{
                    self.notificationArray.remove(at: indexPath.row)
                    self.notificationTV.deleteRows(at: [indexPath], with: .none)
                }
            })
            completion(true)
        })
        action.accessibilityFrame = CGRect(x: 0, y: 0, width: 50, height: 50)
        action.backgroundColor = .red
        return action
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    
}
