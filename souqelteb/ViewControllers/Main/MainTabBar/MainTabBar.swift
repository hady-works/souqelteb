//
//  MainTabBar.swift
//  souqelteb
//
//  Created by HadyHammad on 12/22/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

protocol IndexChangeDelegate {
    func changeTabBarIndex(index: Int)
}

class MainTabBar: UITabBarController, IndexChangeDelegate{
    
    // MARK:- Instance
    static func instance () -> MainTabBar{
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "MainTabBar") as! MainTabBar
    }
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        SideMenuVC.delegate = self
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tabBar.items?[0].title = "Home".localized
        tabBar.items?[1].title = "Notifications".localized
        tabBar.items?[2].title = "Orders".localized
        tabBar.items?[3].title = "Categories".localized
    }
   
    // MARK:- Confirm Protocol
    func changeTabBarIndex(index: Int) {
        selectedIndex = index
    }
    
}
