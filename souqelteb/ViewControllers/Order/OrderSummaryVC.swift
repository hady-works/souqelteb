//
//  OrderSummaryVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/9/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

protocol ChangeAddressDelegate {
    func changeAddress(city: City, area: Area, address: String)
}

class OrderSummaryVC: BaseViewController,ChangeAddressDelegate {
    
    // MARK:- Instance
    static func instance () -> OrderSummaryVC{
        let storyboard = UIStoryboard.init(name: "Order", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "OrderSummaryVC") as! OrderSummaryVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var summaryView: UIView!
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userAddressLbl: UILabel!
    @IBOutlet weak var userPhoneLbl: UILabel!
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var paymentBtn: UIButton!
    
    // MARK:- Instance Variables
    var city:City?
    var area:Area?
    var cartArray:[Cart]?
    var cities:[City]?
    var areas:[Area]?
    var orderArray = [[String:Any]]()
    var orderQuantities:[Int:Int]?
    var orderTotalPrice:Double?
    var address:String?
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
        loadUserData()
        mapCartToOrder(cart: self.cartArray)
    }
    
    override func viewDidLayoutSubviews() {
        self.paymentView.roundedFromSide(corners: [.topLeft, .topRight], cornerRadius: 18)
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        summaryView.addCornerRadius(8)
        summaryView.addBorderWith(width: 1.5, color: UIColor.borderColor)
        summaryView.addNormalShadow()
        paymentBtn.addCornerRadius(8)
    }
    
    func loadUserData(){
        self.showLoadingInticator()
        guard let userID = NetworkHelper.getUserId() else{return}
        let url = Constants.baseURL + "\(userID)/getUser"
        API.sendRequest(method: .get, url: url, completion: { (err,status,response: Response?) in
            self.hideLoadingInticator()
            if err == nil{
                if status!{
                    guard let user = response?.user else {return}
                    guard let fname = user.firstname else {return}
                    guard let lname = user.lastname else {return}
                    guard let address = user.address else {return}
                    guard let cityName = user.city?.cityName else {return}
                    guard let areaName = user.area?.areaName else {return}
                    //... Update UI
                    DispatchQueue.main.async {
                        self.city = user.city
                        self.area = user.area
                        self.userNameLbl.text = fname+" "+lname
                        self.userPhoneLbl.text = user.phone
                        self.userAddressLbl.text = "\(cityName), \(areaName), \(address)"
                        self.address = address
                    }
                }
            }
        })
    }
    
    func mapCartToOrder(cart: [Cart]?){
        guard let cart = cart else{return}
        for item in cart{
            
            guard let productID = item.product?.id,
                let qty = self.orderQuantities?[productID] else{return}
            
            guard let color = item.color, let size = item.size else{
                self.hideLoadingInticator()
                return
            }
            
            let dict = [ "product": productID,
                         "sizes": size,
                         "color": color,
                         "count": qty
                ] as [String : Any]
            self.orderArray.append(dict)
        }
    }
    
    func changeAddress(city: City, area: Area, address: String) {
        self.city = city
        self.area = area
        self.userAddressLbl.text = address
    }
    
    // MARK:- Action
    @IBAction func buChangeAddress(_ sender: Any) {
        let changeAddressVC = ChangeAddressVC.instance()
        changeAddressVC.delegate = self
        changeAddressVC.selectedCity = self.city
        changeAddressVC.selectedArea = self.area
        changeAddressVC.address = self.address
        self.present(changeAddressVC, animated: true, completion: nil)
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buPayment(_ sender: Any) {
        let paymentVC = PaymentVC.instance()
        paymentVC.city = self.city?.id
        paymentVC.area = self.area?.id
        paymentVC.address = self.userAddressLbl.text!
        paymentVC.orderArray = self.orderArray
        paymentVC.totalPrice = self.orderTotalPrice
        self.present(paymentVC, animated: true, completion: nil)
    }
}
