//
//  ChangeAddressVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/9/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import MOLH

class ChangeAddressVC: BaseViewController {
    // MARK:- Instance
    static func instance () -> ChangeAddressVC{
        let storyboard = UIStoryboard.init(name: "Order", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ChangeAddressVC") as! ChangeAddressVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var cityTxtField: UITextField!
    @IBOutlet weak var areaTxtField: UITextField!
    @IBOutlet weak var addressTxtView: UITextView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var citiesTable: UITableView!
    @IBOutlet weak var areasTable: UITableView!
    @IBOutlet weak var saveChangesBtn: UIButton!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var areaView: UIView!
    @IBOutlet weak var cityHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var areaHeightConstraint: NSLayoutConstraint!
    
    //MARK:- Instance Variables
    var cities = [City]()
    var areas = [Area]()
    var cityMenu = true
    var areaMenu = true
    var selectedCity:City?
    var selectedArea:Area?
    var orderArray:[[String:Any]]?
    var address:String?
    var delegate:ChangeAddressDelegate?
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        if let city = self.selectedCity?.id{
            DispatchQueue.main.async {
                self.loadCities()
                self.loadAreas(city: city)
            }
        }
        addressTxtView.text = address
        cityTxtField.text  = MOLHLanguage.isArabic() ?  selectedCity?.arabicCityName : selectedCity?.cityName
        areaTxtField.text  = MOLHLanguage.isArabic() ?  selectedArea?.arabicAreaName : selectedArea?.areaName
        cityTxtField.delegate = self
        areaTxtField.delegate = self
        addressTxtView.delegate = self
        citiesTable.isHidden = true
        areasTable.isHidden = true
        cityView.addBorderWith(width: 1.5, color:  UIColor.borderColor)
        cityView.addCornerRadius(8)
        areaView.addBorderWith(width: 1.5, color:  UIColor.borderColor)
        areaView.addCornerRadius(8)
        saveChangesBtn.addBtnCornerRadius(5)
        saveChangesBtn.addBtnShadowWith(color: UIColor.black, radius: 2, opacity: 0.2)
        addressView.addCornerRadius(8)
        addressView.addBorderWith(width: 1.5, color: UIColor.borderColor)
        addressView.addShadowWith(color: UIColor.shadowColor, radius: 3, opacity: 0.6)
        addressTxtView.textColor = .lightGray
        addressTxtView.text = "Address".localized
    }
   
    // MARK:- DropDown Menus
    func loadCities(){
        
        API.load_location(completion: { (err, cities: [City]?) in
            if err == nil{
                guard let cities = cities else{return}
                self.cityHeightConstraint.constant = CGFloat(cities.count * 40)
                self.cities = cities
                DispatchQueue.main.async {
                    self.citiesTable.reloadData()
                }
            }else{
                self.showAlertError(title: "There is error may be connection lost".localized)
            }
        })
        
    }
    
    func loadAreas(city: Int){
        API.load_location(cityID: city, completion: { (err, areas: [Area]?) in
            if err == nil{
                guard let areas = areas else{return}
                //self.areaHeightConstraint.constant = CGFloat(areas.count * 40)
                self.areas = areas
                DispatchQueue.main.async {
                    self.areasTable.reloadData()
                }
            }else{
                self.showAlertError(title: "There is error may be connection lost".localized)
            }
        })
    }
    
    @IBAction func buCityMenu(_ sender: Any) {
        if cityMenu{
            UIView.animate(withDuration: 0.2, animations: {
                self.addressView.layer.borderColor = UIColor.borderColor.cgColor
                self.cityView.layer.borderColor = UIColor.selectedBorderColor.cgColor
                self.areaView.layer.borderColor = UIColor.borderColor.cgColor
                self.citiesTable.isHidden = false
                self.cityMenu = false
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.cityView.layer.borderColor = UIColor.borderColor.cgColor
                self.citiesTable.isHidden = true
                self.cityMenu = true
            })
        }
    }
    
    @IBAction func buAreaMenu(_ sender: Any) {
        if let city = self.selectedCity?.id{

            API.load_location(cityID: city, completion: { (err, areas: [Area]?) in
                if err == nil{
                    guard let areas = areas else{return}
                    self.areaHeightConstraint.constant = CGFloat(areas.count * 40)
                    self.areas = areas
                    DispatchQueue.main.async {
                        self.areasTable.reloadData()
                    }
                }else{
                    self.showAlertError(title: "There is error may be connection lost".localized)
                }
            })
            
            if areaMenu{
                UIView.animate(withDuration: 0.2, animations: {
                    self.addressView.layer.borderColor = UIColor.borderColor.cgColor
                    self.areaView.layer.borderColor = UIColor.selectedBorderColor.cgColor
                    self.cityView.layer.borderColor = UIColor.borderColor.cgColor
                    self.areasTable.isHidden = false
                    self.areaMenu = false
                })
            }else{
                UIView.animate(withDuration: 0.2, animations: {
                    self.areaView.layer.borderColor = UIColor.borderColor.cgColor
                    self.areasTable.isHidden = true
                    self.areaMenu = true
                })
            }
            
        }else{
            self.showAlertWiring(title: "Please select city".localized)
        }
        
    }
    
    // MARK:- Action
    @IBAction func buSaveChanges(_ sender: Any) {
        if validData(){
            guard let address = addressTxtView.text else {return}
            guard let city = MOLHLanguage.isArabic() ?  selectedCity?.arabicCityName : selectedCity?.cityName,
                let area = MOLHLanguage.isArabic() ?  selectedArea?.arabicAreaName : selectedArea?.areaName else{return}
            let fullAddress = "\(city), \(area), \(address)"
            if delegate != nil{
                delegate?.changeAddress(city: self.selectedCity!, area: self.selectedArea!, address: fullAddress)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension ChangeAddressVC {
    // MARK:- Data Validations
    func validData() ->Bool{
        
        if cityTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please select city".localized)
            return false
        }
        
        if areaTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please select area".localized)
            return false
        }
        
        if addressTxtView.text!.isEmpty || addressTxtView.text! == "Address".localized {
            self.showAlertWiring(title: "Please enter address".localized)
            return false
        }
        
        return true
    }
    
    func finish(){
        self.cityTxtField.text = ""
        self.areaTxtField.text = ""
        self.addressTxtView.text = ""
    }
}

extension ChangeAddressVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == areasTable{
            return areas.count
        }else{
            return cities.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == areasTable{
            if let cell = areasTable.dequeueReusableCell(withIdentifier: "areasCell"){
                let area = areas[indexPath.row]
                cell.textLabel?.text  = MOLHLanguage.isArabic() ?  area.arabicAreaName : area.areaName
                return cell
            }else{
                return UITableViewCell()
            }
        }else{
            if let cell = citiesTable.dequeueReusableCell(withIdentifier: "citiesCell"){
                let city = cities[indexPath.row]
                cell.textLabel?.text  = MOLHLanguage.isArabic() ?  city.arabicCityName : city.cityName
                return cell
            }else{
                return UITableViewCell()
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == citiesTable{
            self.selectedArea = nil
            self.areaTxtField.text = nil
            self.selectedCity = self.cities[indexPath.row]
            self.cityTxtField.text = MOLHLanguage.isArabic() ? self.cities[indexPath.row].arabicCityName : self.cities[indexPath.row].cityName
            
            UIView.animate(withDuration: 0.2, animations: {
                self.citiesTable.isHidden = true
                self.cityMenu = true
            })
            
        }else{
            self.selectedArea = self.areas[indexPath.row]
            self.areaTxtField.text = MOLHLanguage.isArabic() ? self.areas[indexPath.row].arabicAreaName : self.areas[indexPath.row].areaName
            
            UIView.animate(withDuration: 0.2, animations: {
                self.areasTable.isHidden = true
                self.areaMenu = true
            })
        }
    }
    
}

extension ChangeAddressVC: UITextFieldDelegate,UITextViewDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == cityTxtField{
            cityView.layer.borderColor = UIColor.selectedBorderColor.cgColor
            areaView.layer.borderColor = UIColor.borderColor.cgColor
            self.areasTable.isHidden = true
        }else if textField == areaTxtField{
            areaView.layer.borderColor = UIColor.selectedBorderColor.cgColor
            cityView.layer.borderColor = UIColor.borderColor.cgColor
            self.citiesTable.isHidden = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == cityTxtField{
            cityView.layer.borderColor = UIColor.borderColor.cgColor
        }else if textField == areaTxtField{
            areaView.layer.borderColor = UIColor.borderColor.cgColor
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
            addressView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }else{
            addressView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Address".localized
            textView.textColor = .lightGray
            addressView.layer.borderColor = UIColor.borderColor.cgColor
        }else{
            addressView.layer.borderColor = UIColor.borderColor.cgColor
        }
    }
    
}
