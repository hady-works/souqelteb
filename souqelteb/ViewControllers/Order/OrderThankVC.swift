//
//  OrderThankVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/15/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class OrderThankVC: UIViewController {
    
    // MARK:- Instance
    static func instance () -> OrderThankVC{
        let storyboard = UIStoryboard.init(name: "Order", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "OrderThankVC") as! OrderThankVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var orderNumberLbl: UILabel!
    @IBOutlet weak var amountPaidLbl: UILabel!
    @IBOutlet weak var orderTV: UITableView!
    
    // MARK:- Variables
    var order:Order?
    var productOrders:[ProductOrders]?
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
        setupOrders()
    }

    override func viewDidLayoutSubviews() {
        self.topView.roundedFromSide(corners: [.topLeft, .topRight], cornerRadius: 8)
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        orderTV.registerCellNib(cellClass: OrderTVC.self)
        topView.addBorderWith(width: 1, color: UIColor.borderColor)
        topView.addNormalShadow()
    }
    
    func setupOrders(){
        guard let orderProducts = self.order?.productOrders else{return}
        guard let id = self.order?.id else{return}
        guard let total = self.order?.finalTotal else{return}
        self.productOrders = orderProducts
        self.orderNumberLbl.text = "\(id)"
        self.amountPaidLbl.text = "\(total)"
    }
    
    // MARK:- Action
    @IBAction func buDone(_ sender: Any) {
        let orderVC = MainTabBar.instance()
        orderVC.selectedIndex = 2
        self.present(orderVC, animated: true, completion: nil)
    }
    
}

extension OrderThankVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let productOrders = self.productOrders {
            return productOrders.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = orderTV.dequeue() as OrderTVC
        if let productOrders = self.productOrders {
            cell.setupSummaryComponent()
            cell.configure(order: productOrders[indexPath.row])
            return cell
        }else{
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
}

