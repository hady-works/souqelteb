//
//  PaymentVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/9/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import MOLH

class PaymentVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> PaymentVC{
        let storyboard = UIStoryboard.init(name: "Order", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var cashView: UIView!
    @IBOutlet weak var cashImageView: UIImageView!
    
    //MARK: - Instance Variables
    var city:Int?
    var area:Int?
    var address:String?
    var orderArray:[[String:Any]]?
    var totalPrice:Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        paymentView.addCornerRadius(8)
        paymentView.addBorderWith(width: 1.5, color: UIColor.borderColor)
        paymentView.addNormalShadow()
        cashView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cashTapped)))
        if MOLHLanguage.isArabic(){
            self.cashImageView.image = UIImage(named: "keyboard_arrow_left")
        }else{
            self.cashImageView.image = UIImage(named: "keyboard_arrow_right")
        }
    }
    
    @objc func cashTapped(){
        makeOrder(paymentSystem: "cash")
    }
    
    func makeOrder(paymentSystem: String){
        guard let area    = self.area else{return}
        guard let city    = self.city else{return}
        guard let address = self.address else{return}
        guard let orderValues = self.orderArray else{return}
        guard let id = NetworkHelper.getUserId() else{return}
        
        let order = [ "destination": [1.3, 1.5],
                      "productOrders": orderValues,
                      "city": city,
                      "area": area,
                      "address": address,
                      "paymentSystem": paymentSystem
            ] as [String : Any]
        
        
        let url = Constants.order+"\(id)/users"
        self.showLoadingInticator()
        API.sendOrder(url: url, parameters: order, completion: { (err, status, response:Order?) in
            print("uiqiup: ", response)
            self.hideLoadingInticator()
            if err == nil{
                if status!{
                    guard let order = response else{return}
                    self.successSentOrder(order: order)
                }
            }
        })
    }
    
    func successSentOrder(order: Order){
        self.city = nil
        self.area = nil
        self.address = nil
        self.orderArray = nil
        NetworkHelper.cart = []
        self.showAlertsuccess(title: "Order sent success".localized)
        let orderThankVC = OrderThankVC.instance()
        orderThankVC.order = order
        self.present(orderThankVC, animated: true, completion: nil)
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
