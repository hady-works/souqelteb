//
//  MyCartVC.swift
//  souqelteb
//
//  Created by HadyHammad on 1/6/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import MOLH

protocol ChangePriceDelegate {
    func changeQuantity(id: Int, Qty: Int, warning: Bool)
}
class MyCartVC: BaseViewController, ChangePriceDelegate {
    
    //MARK:- Instance
    static func instance () -> MyCartVC{
        let storyboard = UIStoryboard.init(name: "Order", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "MyCartVC") as! MyCartVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var cartTV: UITableView!
    @IBOutlet weak var orderView: UIView!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var deliveryPriceLbl: UILabel!
    @IBOutlet weak var orderBtn: UIButton!
    @IBOutlet weak var emptyStateLbl: UILabel!
    
    //MARK:- Instance Variables
    var cartArray:[Cart]?
    var totalPrice:Double = 0
    var quantityArray = [Int:Int]()
    var orderArray = [[String:Any]]()
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
        getCartProducts()
    }
    
    override func viewDidLayoutSubviews() {
        self.orderView.roundedFromSide(corners: [.topLeft, .topRight], cornerRadius: 18)
    }
    
    //MARK:- SetupUI
    func setupComponents() {
        cartTV.registerCellNib(cellClass: CartTVC.self)
        cartTV.isHidden = true
        orderView.isHidden = true
        cartTV.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
        orderBtn.addCornerRadius(8)
    }
    
    func changeQuantity(id: Int, Qty: Int, warning: Bool) {
        if warning {
            self.showAlertWiring(title: "\(Qty) " + "products only available in the stock".localized )
        }else{
            guard let cartArray = self.cartArray else{return}
            self.orderArray.removeAll()
            self.quantityArray[id] = Qty
            self.mapCartToOrder(cart: cartArray)
        }
    }
    
    func getCartProducts() {
        
        guard let userID = NetworkHelper.getUserId() else{return}
        let url = Constants.cart+"\(userID)/users"
        
        self.showLoadingInticator()
        API.sendRequest(method: .get, url: url, completion: { (err,status,response: CartResponse?) in
            
            if err == nil{
                if status!{
                    
                    guard let cartArray = response?.data else{
                        self.hideLoadingInticator()
                        return
                    }
                    
                    if cartArray.count == 0{
                        self.emptyStateLbl.isHidden = false
                    }
                    
                    self.cartArray = cartArray
                    
                    for item in cartArray{
                        
                        guard let id = item.product?.id else{
                            self.hideLoadingInticator()
                            return
                        }
                        
                        self.quantityArray[id] = 1
                    }
                    
                    self.mapCartToOrder(cart: cartArray)
                    self.cartTV.isHidden = false
                    self.orderView.isHidden = false
                    DispatchQueue.main.async {
                        self.cartTV.reloadData()
                    }
                }
            }else{
                self.hideLoadingInticator()
                self.emptyStateLbl.isHidden = false
            }
        })
    }
    
    func mapCartToOrder(cart: [Cart]){
        for item in cart{
            guard let productID = item.product?.id else{
                self.hideLoadingInticator()
                return
            }
            
            guard let color = item.color, let size = item.size else{
                self.hideLoadingInticator()
                return
            }
            
            guard let qty = self.quantityArray[productID] else{
                self.hideLoadingInticator()
                return
            }
            
            let dict = [ "product": productID,
                         "sizes": size,
                         "color": color,
                         "count": qty
                ] as [String : Any]
            
            self.orderArray.append(dict)
        }
        getDelivaryPrice(order: self.orderArray)
    }
    
    func getDelivaryPrice(order: [[String:Any]] ){
        
        let order = [ "productOrders": order,
                      "city": NetworkHelper.getCityId() ?? 1,
                      "area": NetworkHelper.getAreaId() ?? 1 ] as [String : Any]
        
        API.sendOrder(url: Constants.delivaryCost, parameters: order, completion: { (err,status,response: DeliveryCost?) in
            self.hideLoadingInticator()
            if err == nil{
                if status!{
                    guard let delivaryCost = response else{return}
                    self.totalLbl.text = "Total".localized + "  \(delivaryCost.finalTotal ?? 0)"
                    self.deliveryPriceLbl.text = "Delivery price".localized + "  \(delivaryCost.delivaryCost ?? 0)"
                }
            }
        })
    }
    
    //MARK:- Actions
    @IBAction func buOrder(_ sender: Any) {
        if self.cartArray?.count != 0 {
            let orderSummary = OrderSummaryVC.instance()
            orderSummary.cartArray = self.cartArray
            orderSummary.orderQuantities = self.quantityArray
            orderSummary.orderTotalPrice = self.totalPrice
            self.present(orderSummary, animated: true, completion: nil)
        }else{
            self.showAlertWiring(title: "Cart is empty".localized)
        }
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension MyCartVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let cartArray = self.cartArray {
            return cartArray.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cartTV.dequeue() as CartTVC
        if let cartArray = self.cartArray {
            cell.configure(product: cartArray[indexPath.row].product)
            cell.delegate = self
            if let id = cartArray[indexPath.row].product?.id{
                cell.removeFromCartBtn.tag = id
                cell.removeFromCartBtn.addTarget(self, action: #selector(buRemoveItemPress(_:)), for: .touchUpInside)
            }
            return cell
        }else{
            return cell
        }
    }
    
    @objc func buRemoveItemPress(_ sender:UIButton){
        API.sendRequest(userImage: nil, method: .delete, url: Constants.cart+"\(sender.tag)", parameters: nil, header: Constants.header, completion: { (err,status,response: Response?) in
            if err == nil{
                if status!{
                    guard let arr = response?.user?.carts else{return}
                    NetworkHelper.cart = arr
                    self.cartArray?.removeAll{$0.product?.id == sender.tag}
                    self.orderArray.removeAll()
                    self.quantityArray.removeValue(forKey: sender.tag)
                    guard let cartArray = self.cartArray else{return}
                    self.mapCartToOrder(cart: cartArray)
                    if self.cartArray?.count == 0{
                        self.emptyStateLbl.isHidden = false
                    }
                    self.cartTV.reloadData()
                }
            }
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
}
