//
//  ContactUsVC.swift
//  souqelteb
//
//  Created by Mac on 12/13/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class ContactUsVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> ContactUsVC{
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var firstnameTxtField: CustomTxtField!
    @IBOutlet weak var lastnameTxtField: CustomTxtField!
    @IBOutlet weak var emailTxtField: CustomTxtField!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageTxtView: LocalizedTextView!
    @IBOutlet weak var phoneTxtField: CustomTxtField!
    @IBOutlet weak var sendBtnOutlet: UIButton!
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    // MARK:- SetupUI
    func setupComponents(){
        firstnameTxtField.delegate = self
        lastnameTxtField.delegate = self
        emailTxtField.delegate = self
        phoneTxtField.delegate = self
        messageTxtView.delegate = self
        sendBtnOutlet.addBtnCornerRadius(8)
        sendBtnOutlet.addBtnShadowWith(color: UIColor.black, radius: 2, opacity: 0.2)
        messageView.addCornerRadius(8)
        messageView.addBorderWith(width: 1.5, color: UIColor.borderColor)
        messageView.addShadowWith(color: UIColor.shadowColor, radius: 3, opacity: 0.6)
        messageTxtView.textColor = .lightGray
        messageTxtView.text = "Message".localized
    }
    
    // MARK:- Action
    @IBAction func sendActionBtn(_ sender: Any) {
        if validData(){
            let parameters =  [
                "name":    self.firstnameTxtField.text!+" "+self.lastnameTxtField.text!,
                "number":                                      self.phoneTxtField.text!,
                "email":                                       self.emailTxtField.text!,
                "message":                                    self.messageTxtView.text!,
            ]
            
            API.ContactUs(userData: parameters, completion: { (err,status) in
                if (err == nil){
                    if status!{
                        self.showAlertsuccess(title: "Sent success".localized)
                        self.finish()
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        self.showAlertWiring(title: "Check your data again".localized)
                    }
                }else{
                    self.showAlertError(title: "There is error may be connection lost".localized)
                }
            })
            
        }
    }
    
    @IBAction func buBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- Data Validations
    func validData() -> Bool{
        
        if firstnameTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter first name".localized)
            return false
        }
        
        if firstnameTxtField.text!.count < 3{
            self.showAlertWiring(title: "Enter correct first name".localized)
            return false
        }
        
        if lastnameTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter last name".localized)
            return false
        }
        
        if lastnameTxtField.text!.count < 3{
            self.showAlertWiring(title: "Enter correct last name".localized)
            return false
        }
        
        if emailTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter email".localized)
            return false
        }
        
        if !(emailTxtField.text!.isValidEmail){
            self.showAlertWiring(title: "Enter valid email".localized)
            return false
        }
        
        if phoneTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter phone".localized)
            return false
        }
        
        if phoneTxtField.text!.count != 11{
            self.showAlertWiring(title: "Phone number must be 11 digits".localized)
            return false
        }
        
        if messageTxtView.text!.isEmpty{
            self.showAlertWiring(title: "Please enter message".localized)
            return false
        }
        
        return true
    }
    
    func finish(){
        self.firstnameTxtField.text = ""
        self.lastnameTxtField.text = ""
        self.emailTxtField.text = ""
        self.phoneTxtField.text = ""
        self.messageTxtView.text = ""
    }
    
}

extension ContactUsVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.selectedBorderColor.cgColor
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.borderColor.cgColor
    }
}

extension ContactUsVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
            messageView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }else{
            messageView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Message".localized
            textView.textColor = .lightGray
            messageView.layer.borderColor = UIColor.borderColor.cgColor
        }else{
            messageView.layer.borderColor = UIColor.borderColor.cgColor
        }
    }
}
