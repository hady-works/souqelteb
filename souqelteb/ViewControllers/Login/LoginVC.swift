//
//  ViewController.swift
//  souqelteb
//
//  Created by Mac on 12/3/19.
//  Copyright © 2019 Mac. All rights reserved.
//
import UIKit
import Alamofire

class LoginVC: BaseViewController {
    
    // MARK:- Instance
    static func instance () -> LoginVC{
        let storyboard = UIStoryboard.init(name: "Login", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
    }
    
    // MARK:- Outlets
    @IBOutlet weak var emailTxtField: CustomTxtField!
    @IBOutlet weak var passwordTxtField: CustomTxtField!
    @IBOutlet weak var visibility: UIButton!
    @IBOutlet weak var rememberMeBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var passwordView: UIView!
    
    // MARK:- Variables
    var secure = true
    var remember = false
    
    // MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    func setupComponents(){
        emailTxtField.delegate = self
        passwordTxtField.delegate = self
        passwordView.addBorderWith(width: 1.5, color: UIColor.borderColor)
        passwordView.addCornerRadius(8)
        loginBtn.addBtnCornerRadius(8)
        skipBtn.addBtnCornerRadius(8)
        skipBtn.addBtnShadowWith(color: UIColor.black, radius: 2, opacity: 0.2)
        loginBtn.addBtnShadowWith(color: UIColor.black, radius: 2, opacity: 0.2)
        signUpBtn.underline()
    }
    
    // MARK:- Action's
    @IBAction func buSignUp(_ sender: Any) {
        self.present(SignupVC.instance(), animated: true, completion: nil)
    }
    
    @IBAction func buForgetPassword(_ sender: Any) {
        self.present(ForgetPasswordVC.instance(), animated: true, completion: nil)
    }
    
    @IBAction func buRememberMe(_ sender: Any) {
        if !remember{
            rememberMeBtn.setImage(UIImage(named: "check_box"), for: .normal)
            remember = true
        }else{
            rememberMeBtn.setImage(UIImage(named: "check_box-1"), for: .normal)
            remember = false
        }
    }
    
    @IBAction func buShowPassword(_ sender: Any) {
        if secure{
            passwordTxtField.isSecureTextEntry = false
            secure = false
            visibility.setImage(UIImage(named: "visibility"), for: .normal)
        }else{
            passwordTxtField.isSecureTextEntry = true
            secure = true
            visibility.setImage(UIImage(named: "visibility_off"), for: .normal)
        }
    }
    
    @IBAction func buLogin(_ sender: Any) {
        guard let email = emailTxtField.text, let password = passwordTxtField.text else{return}
        let parameter = [ "email": email, "password": password, "token": ""]
        if validData(){
            self.showLoadingInticator()
            API.sendRequest(method: .post, url: Constants.sign_in, parameters: parameter, completion: { (err,status,response: LoginResponse?) in
                self.hideLoadingInticator()
                if err == nil{
                    if status!{
                        guard let response = response else{return}
                        self.successLogin(response: response)
                    }
                }else{
                    if err!.localizedDescription == "The request timed out."{
                        self.showAlertError(title: "Network connection lost".localized)
                    }else{
                        self.showAlertWiring(title: "Login faild".localized)
                    }
                }
            })
        }
    }
    
    @IBAction func buSkip(_ sender: Any) {
        self.present(MainTabBar.instance(), animated: true, completion: nil)
    }
    
    // MARK:- Api Calling
    func successLogin(response: LoginResponse){
        self.showAlertsuccess(title: "Login success".localized)
        
        if let user = response.user {
            guard let fname   = user.firstname else{return}
            guard let lname   = user.lastname else{return}
            guard let accessToken   = response.token else{return}
            
            NetworkHelper.userID        = user.id
            NetworkHelper.accessToken   = accessToken
            NetworkHelper.userEmail     = user.email
            NetworkHelper.favourite     = user.favourite ?? []
            NetworkHelper.cart          = user.carts ?? []
            NetworkHelper.userImage     = user.img
            NetworkHelper.city          = user.city?.id
            NetworkHelper.area          = user.area?.id
            NetworkHelper.userName      = fname+" "+lname
        }
        
        if self.remember{
            Constants.isLogIn = true
        }
        self.finish()
        self.present(MainTabBar.instance(), animated: true, completion: nil)
    }
    
    // MARK:- Data Validations
    func validData() -> Bool{
        
        if emailTxtField.text!.isEmpty && passwordTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter your data".localized)
            return false
        }
        
        if emailTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter email".localized)
            return false
        }
        
        if !(emailTxtField.text!.isValidEmail){
            self.showAlertWiring(title: "Enter valid email".localized)
            return false
        }
        
        if passwordTxtField.text!.isEmpty{
            self.showAlertWiring(title: "Please enter password".localized)
            return false
        }
        
        if passwordTxtField.text!.count <= 5{
            self.showAlertWiring(title: "Password is less than 6".localized)
            return false
        }
        return true
    }
    
    func finish(){
        self.emailTxtField.text = ""
        self.passwordTxtField.text = ""
    }
    
}

// MARK:- Extension's
extension LoginVC: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == passwordTxtField{
            passwordView.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }else{
            textField.layer.borderColor = UIColor.selectedBorderColor.cgColor
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == passwordTxtField{
            passwordView.layer.borderColor = UIColor.borderColor.cgColor
        }else{
            textField.layer.borderColor = UIColor.borderColor.cgColor
        }
    }
    
}
