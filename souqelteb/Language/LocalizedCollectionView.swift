//
//  LocalizedCollectionView.swift
//  souqelteb
//
//  Created by Mac on 12/3/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import MOLH
class LocalizedCollectionView: UICollectionViewFlowLayout {
    override var flipsHorizontallyInOppositeLayoutDirection: Bool {
        return MOLHLanguage.isArabic()
    }
    
}

