//
//  LocalizedButton.swift
//  souqelteb
//
//  Created by Mac on 12/3/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import MOLH

class LocalizedButton: UIButton{
    
    override func awakeFromNib() {
        self.setTitle(self.currentTitle?.localized, for: .normal)
    }
    
}
