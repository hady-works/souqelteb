//
//  LocalizedLabel.swift
//  souqelteb
//
//  Created by Mac on 12/3/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import MOLH

class LocalizedLabel: UILabel{
    
    override func awakeFromNib() {
        if let txt = self.text{
            self.text = txt.localized
        }
        adjustAutoAlignment()
    }
    
    
    
    func adjustAutoAlignment(){
        if self.textAlignment != .center {
            if MOLHLanguage.isArabic() {
                self.textAlignment = .right
            }else{
                self.textAlignment = .left
            }
        }
    }
}
