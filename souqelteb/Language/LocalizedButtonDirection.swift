//
//  LocalizedButtonFlippedDirection.swift
//  souqelteb
//
//  Created by Mac on 12/3/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import MOLH

class LocalizedButtonDirection: UIButton{
    
    override func awakeFromNib() {
        flippedDirection()
    }
    func flippedDirection(){
        if MOLHLanguage.isArabic() {
            self.imageView?.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi));
        }
    }
}
