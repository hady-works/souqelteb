//
//  TermsAndConditionsApi.swift
//  souqelteb
//
//  Created by Mac on 12/14/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import Alamofire

class TermsAndConditionsApi{
    
    static var shared = TermsAndConditionsApi()
    
    func getMyJusoorProfileData(completion: @escaping(_ isSuccess: Bool, _ msg: String, _ result: TermsAndConditionsModel?) -> Void) {
        
        API.generalSendRequest(method: .get, webService: "about", param: [:]) { (reualt) in
            if let res = reualt, let data = res as? JSONArray{
                if let resData = data.first {
                     let abountData = TermsAndConditionsModel.DTO(data: resData)
                     completion(true, "Success".localized, abountData)
                }else {
                    //InvalidData
                    completion(false, "InvalidData".localized, nil)
                }
            }else {
                //InvalidData
                completion(false, "Fatel Error, Check Internet Connection".localized, nil)
            }
        }
    }
}
