//
//  CustomProfileTopView.swift
//  souqelteb
//
//  Created by HadyHammad on 12/24/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
class CustomProfileTopView:UIView{
    override func layoutSubviews() {
        super.layoutSubviews()
        roundedFromSide(corners: [.bottomRight,.bottomLeft], cornerRadius: 10)
    }
}

