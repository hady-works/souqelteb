//
//  UiGradientView.swift
//  souqelteb
//
//  Created by Mac on 12/3/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

@IBDesignable
class UiGradientView: UIView {
    
    
    @IBInspectable var firstColor: UIColor = UIColor.clear{
        didSet{
            updateView()
        }
    }
    @IBInspectable var secondColor: UIColor = UIColor.clear {
        didSet{
            updateView()
        }
    }
    @IBInspectable var startPoint: CGPoint = CGPoint(x: 0, y: 0) {
        didSet{
            updateView()
        }
    }
    
    @IBInspectable var endPoint: CGPoint = CGPoint(x: 1, y: 1) {
        didSet{
            updateView()
        }
    }
    
    
    
    override class var layerClass: AnyClass {
        get{
            return CAGradientLayer.self
        }
    }
    
    func updateView() {
        
        let layer = self.layer as! CAGradientLayer
        layer.colors = [firstColor.cgColor , secondColor.cgColor]
        //        layer.locations = [0.5]
        layer.startPoint = startPoint
        layer.endPoint = endPoint
    }
    
}
