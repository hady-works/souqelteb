//
//  RoundedImageView.swift
//  souqelteb
//
//  Created by HadyHammad on 12/24/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit
class RoundedImageView:UIImageView{
    
    @IBInspectable var cornerRadius:CGFloat = 0{
        didSet{
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
}

import UIKit
import MOLH
@IBDesignable

class LocalizedTextField: UITextField {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.tintColor = .lightGray
        if MOLHLanguage.isArabic() {
            if let placeHolder = self.placeholder {
                self.placeholder = placeHolder.localized
            }
        }
        
        adjustAutoAlignment()
    }
    
    
    func adjustAutoAlignment(){
        if self.textAlignment != .center {
            if MOLHLanguage.isArabic() {
                self.textAlignment = .right
            }else{
                
                self.textAlignment = .left
            }
        }
    }
}

