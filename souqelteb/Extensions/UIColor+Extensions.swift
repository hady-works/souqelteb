//
//  UIColor+Extensions.swift
//  souqelteb
//
//  Created by HadyHammad on 12/10/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

extension UIColor{
    static var borderColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
    static var selectedBorderColor = UIColor(red: 52/255, green: 151/255, blue: 253/255, alpha: 1)
    static var verifyBorderColor = UIColor(red: 79/255, green: 143/255, blue: 0/255, alpha: 1).cgColor
    static var shadowColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 0.6)
}
