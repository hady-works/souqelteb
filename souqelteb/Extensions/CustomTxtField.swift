//
//  CustomTxtField.swift
//  souqelteb
//
//  Created by Mac on 12/3/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit
import MOLH
@IBDesignable

class CustomTxtField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 40)
    
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.tintColor = .lightGray
        if MOLHLanguage.isArabic() {
            if let placeHolder = self.placeholder {
                self.placeholder = placeHolder.localized
            }
        }
        adjustAutoAlignment()
    }
    
      // Provides left padding for images
      override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
          var textRect = super.leftViewRect(forBounds: bounds)
          textRect.origin.x += leftPadding
          return textRect
      }
      
      override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
          var textRect = super.rightViewRect(forBounds: bounds)
          textRect.origin.x -= rightPadding
          return textRect
      }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var rightPadding: CGFloat = 0
    
    @IBInspectable var color: UIColor = UIColor.white {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor?{
        didSet{
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var cornerRadius:CGFloat = 0{
        didSet{
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    func updateView() {
        
        if let image = leftImage {
                leftViewMode = UITextField.ViewMode.always
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
                imageView.contentMode = .scaleAspectFit
                imageView.image = image
                imageView.tintColor = color
                leftView = imageView
                
            } else {
                leftViewMode = UITextField.ViewMode.never
                leftView = nil
            }
            
            // Placeholder text color
            attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: color])
            
            self.tintColor = .lightGray
            if MOLHLanguage.isArabic() {
                if let placeHolder = self.placeholder {
                    self.placeholder = placeHolder.localized
                }
            }
            
            adjustAutoAlignment()
    }
    
    func adjustAutoAlignment(){
        if self.textAlignment != .center {
            if MOLHLanguage.isArabic() {
                self.textAlignment = .right
            }else{
                
                self.textAlignment = .left
            }
        }
    }
    
}
