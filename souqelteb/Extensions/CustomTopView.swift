//
//  CustomTopView.swift
//  souqelteb
//
//  Created by HadyHammad on 12/10/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
class CustomTopView:UIView{
    override func layoutSubviews() {
        super.layoutSubviews()
        roundedFromSide(corners: [.bottomRight,.bottomLeft], cornerRadius: 25)
    }
}
