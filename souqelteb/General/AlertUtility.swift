//
//  AlertUtility.swift
//  souqelteb
//
//  Created by Mac on 12/3/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class AlertUtility{
    
    typealias MethodHandler = ()  -> Void
    
    class func showAlert(title: String, message: String, VC: UIViewController) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok".localized, style: .cancel, handler: { (alertAction) in
        })
        
        alertVC.addAction(ok)
        VC.present(alertVC, animated: true, completion: nil)
        
    }
    class func showAlertWithCompletion(title: String, message: String, VC: UIViewController, methodHandler: @escaping MethodHandler) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok".localized, style: .cancel, handler: { (alertAction) in
            methodHandler()
        })
        
        alertVC.addAction(ok)
        VC.present(alertVC, animated: true, completion: nil)
        
    }
    
    class   func showWithOkAndCancel(textColor: UIColor, title : String , message: String, cancelTitle : String , otherTitles : [String], completion :@escaping (_ index : Int) -> Void)  {
        
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        
        alertController.view.tintColor = textColor
        let cancelAction = UIAlertAction.init(title: cancelTitle, style: .cancel, handler: { (action) in
            completion(0)
            
        })
        alertController.addAction(cancelAction)
        
        for string in otherTitles {
            
            let action = UIAlertAction.init(title: string, style: .default, handler: { (action) in
                
                completion(otherTitles.firstIndex(of: string)! + 1)
                
            })
            alertController.addAction(action)
            
        }
        //UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
        
    }
}
