//
//  Constants.swift
//  souqelteb
//
//  Created by Mac on 12/4/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit

let NoImageName = "user image"

class Constants{
    //https://souqelteb.herokuapp.com/api/v1/
    static var baseURL = "https://souqelteb.com:2083/api/v1/"
    static let sign_in                  = baseURL + "signin"
    static let sign_up                  = baseURL + "signup"
    static let cities                   = baseURL + "cities"
    static let check_email              = baseURL + "check-exist-email"
    static let check_phone              = baseURL + "check-exist-phone"
    static let forget_password          = baseURL + "sendCode"
    static let confirm_Code             = baseURL + "confirm-code"
    static let reset_password           = baseURL + "reset-password"
    static let update_password          = baseURL + "user/updatePassword"
    static let contact_us               = baseURL + "contact-us"
    static let update_info              = baseURL + "user/"
    static let header                   = ["Authorization": "Bearer \( NetworkHelper.getAccessToken() ?? "" )"]
    static let categories               = baseURL + "categories"
    static let Products                 = baseURL + "products/"
    static let products                 = baseURL + "products?"
    static let topSeller                = products + "topSeller=true"
    static let offers                   = products + "hasOffer=true"
    static let category                 = products + "category="
    static let favourite                = baseURL + "favourite/"
    static let ads                      = baseURL + "ads"
    static let cart                     = baseURL + "cart/"
    static let order                    = baseURL + "orders/"
    static let getOrders                = baseURL + "orders?clientId="
    static let search                   = products + "name="
    static let getNotification          = baseURL + "notif?page="
    static let Notification             = baseURL + "notif"
    static let delivaryCost             = order + "getDelivaryPrice"
    
    static var isLogIn = false{
        didSet{
            UserDefaults.standard.set(isLogIn, forKey: "isLogIn")
            UserDefaults.standard.synchronize()
        }
    }
    
    static func getIsLogIn() -> Bool {
        
        if let logIn = UserDefaults.standard.value(forKey: "isLogIn") as? Bool{
            Constants.isLogIn = logIn
        }
        return Constants.isLogIn
    }
    
}
