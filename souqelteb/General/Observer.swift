//
//  Observer.swift
//  souqelteb
//
//  Created by Mac on 12/3/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
internal final class Observer<Variable> {
    
    typealias Listener = (Variable?)->()
    
    private var listener: Listener?
    
    internal var value: Variable? {
        didSet {
            if value != nil {
                DispatchQueue.global(qos: .userInitiated)
                    .async { [weak self] in
                        self?.listener?(self?.value)
                }
            }
        }
    }
    
    internal func bind(_ listener: Listener?) -> Void {
        DispatchQueue.global(qos: .userInitiated)
            .async { [weak self] in
                self?.listener = listener
                if self?.value != nil {
                    listener?(self?.value)
                }
        }
    }
    
}
