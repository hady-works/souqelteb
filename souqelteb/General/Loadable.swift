//
//  Loadable.swift
//  souqelteb
//
//  Created by Mac on 12/4/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

protocol Loadable {
    func showLoadingInticator(_ message: String, isAppFreezed: Bool)
    func hideLoadingInticator()
    func hideLoadingInticator(withErrorMessage message: String)
}

extension Loadable {
    func hideLoadingInticator(withErrorMessage message: String) {
        
    }
}

extension Loadable where Self: UIViewController {
    
    func showLoadingInticatorFlash(_ message: String, delay: Double) {
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.areDefaultMotionEffectsEnabled = false
        hud.center = self.view.center
        hud.label.text = message
        hud.label.numberOfLines = 0
        hud.contentColor = UIColor.black
        hud.customView = nil
        hud.mode = MBProgressHUDMode.customView
        hud.show(animated: true)
        hud.hide(animated: true, afterDelay: delay)
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func showLoadingInticator(_ message: String = "Loading".localized, isAppFreezed: Bool = false) {
        DispatchQueue.main.async {
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.areDefaultMotionEffectsEnabled = false
            hud.activityIndicatorColor = .selectedBorderColor
            hud.label.font = UIFont(name: "Roboto-medium", size: 16)
            hud.label.text = message
            hud.contentColor = .selectedBorderColor
            hud.isUserInteractionEnabled = false
            if isAppFreezed {
                UIApplication.shared.beginIgnoringInteractionEvents()
            }
        }
    }
    
    func hideLoadingInticator() {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
    func hideLoadingInticator(withErrorMessage message: String) {
        hideLoadingInticator()
        let alert = UIAlertController(title: "Error".localized, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok".localized, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension UITableViewCell: Loadable {
    
    func showLoadingInticator(_ message: String = "", isAppFreezed: Bool = false) {
        let hud = MBProgressHUD.showAdded(to: self.contentView, animated: true)
        hud.areDefaultMotionEffectsEnabled = false
        hud.label.text = message
        hud.contentColor = UIColor.black
        hud.isUserInteractionEnabled = false
    }
    
    func hideLoadingInticator() {
        MBProgressHUD.hide(for: self.contentView, animated: true)
    }
}

extension UICollectionViewCell: Loadable {
    
    func showLoadingInticator(_ message: String = "", isAppFreezed: Bool = false) {
        let hud = MBProgressHUD.showAdded(to: self.contentView, animated: true)
        hud.areDefaultMotionEffectsEnabled = false
        
        hud.label.text = message
        hud.contentColor = UIColor.black
        hud.isUserInteractionEnabled = false
    }
    
    func hideLoadingInticator() {
        MBProgressHUD.hide(for: self.contentView, animated: true)
    }
}

