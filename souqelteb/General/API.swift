//
//  API.swift
//  souqelteb
//
//  Created by HadyHammad on 12/8/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import Alamofire

typealias JSON = [String : Any]
typealias JSONArray = [JSON]

struct NetworkingManager {
    
    static let shared: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        let sessionManager = Alamofire.SessionManager(configuration: configuration)
        return sessionManager
    }()
    
}

class API{
    
    //MARK:- Send Request
    /**
     This Function is used as an abstract to handle API requests using Alamofire
     - parameter method: The HTTP Methoud To Use
     - parameter webService: is the name of service to call like login, register, getData in Type of String
     - parameter param: this is a Dictionary contains all data that you need to send this request
     - parameter showLoading: Do you want to Show Loading Hud ?
     - parameter closure: This is the final result that you can get data from request using it
     */
    class func generalSendRequest(method: HTTPMethod , webService: String, param: [String: Any], closure: @escaping ([[String: Any]]?) -> Void){
        
        let URL1 = Constants.baseURL + webService
        print(URL1)
        //let headers = NetworkHelper.getHeaders()
        
        NetworkingManager.shared.request(URL1, method: method, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            
            switch response.result{
            case .success(_):
                
                if let res = response.result.value as? [[String: Any]] {
                    closure(res)
                }else{
                    closure(nil)
                }
                guard let statusCode = response.response?.statusCode else {return}
                switch statusCode {
                case 401,403:
                    print(1)
                    
                case 500 ... 599:
                    print(1)
                    
                default:
                    break
                }
                
            case .failure(_):
                print(response.result.value)
                
                
                if let res = response.result.value as? [[String: Any]]{
                    closure(res)
                }else{
                    closure(nil)
                }
                guard let statusCode = response.response?.statusCode else {return}
                switch statusCode {
                case 401,403:
                    print(1)
                    
                case 500 ... 599:
                    print(1)
                    
                default:
                    break
                }
            }
            
        }
    }
    
    //MARK:- Check Email And Password
    /**
     This Function is used as an abstract to handle API requests using Alamofire
     - parameter url: is the url of service to call like login, register, getData in Type of String
     - parameter parameters: this is a Dictionary contains all data that you need to send this request
     - parameter closure: This is the final result that you can get data from request using it
     */
    class func checkDataExist( parameters: [String:Any], url: String, completion: @escaping (_ error: Error?, _ status: Bool?)->Void) {
        NetworkingManager.shared.request(url, method: .put, parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .responseJSON { res -> Void in
                switch res.result
                {
                case .failure(let error):
                    completion(error,false)
                case .success(_):
                    guard let json = res.result.value as? Dictionary<String, Any>  else { return }
                    guard let status = json["duplicated"] as? Bool else{return}
                    completion(nil,status)
                }
        }
        
    }
    
    class func ContactUs(userData: Dictionary<String,Any>, completion: @escaping (_ error: Error?, _ status: Bool?)->Void) {
        NetworkingManager.shared.request(Constants.contact_us, method: .post, parameters: userData, encoding: URLEncoding.default, headers: nil)
            .responseJSON { res -> Void in
                switch res.result
                {
                case .failure(let error):
                    completion(error,false)
                case .success(_):
                    if let json = res.result.value as? Dictionary<String, Any>{
                        if let _ = json["errors"] as? Dictionary<String,Any>{
                            completion(nil,false)
                        }
                    }else{
                        completion(nil,true)
                    }
                }
        }
        
    }
    
    class func sendReview(url: String, reviewData: Dictionary<String,Any>, completion: @escaping (_ error: Error?, _ status: Bool?)->Void) {
        NetworkingManager.shared.request(url, method: .put, parameters: reviewData, encoding: JSONEncoding.default, headers: Constants.header)
            .responseJSON { res -> Void in
                switch res.result
                {
                case .failure(let error):
                    completion(error,false)
                case .success(_):
                    if let json = res.result.value as? Dictionary<String, Any>{
                        if let _ = json["errors"] as? Dictionary<String,Any>{
                            completion(nil,false)
                        }
                    }else{
                        completion(nil,true)
                    }
                }
        }
        
    }
    
    class func sendOrder<T: Decodable>(url: String, parameters:[String:Any]?, completion: @escaping (_ error: Error?, _ status: Bool?, _ response: T?)->Void) {
        NetworkingManager.shared.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: Constants.header)
            .responseJSON { res -> Void in
               
                switch res.result
                {
                case .failure(let error):
                    completion(error,false,nil)
                case .success(_):
                    
                    do{
                        if let _ = res.result.value as? Dictionary<String, Any>{
                            guard let data = res.data else { return }
                            let response = try JSONDecoder().decode(T.self, from: data)
                            completion(nil,true,response)
                        }else{
                            completion(nil,false,nil)
                        }
                    }catch let err{
                        print("Error In Decode Data \(err.localizedDescription)")
                        completion(err,false,nil)
                    }
                }
        }
        
    }
    
    //MARK:- Send Request
    /**
     This Function is used as an abstract to handle API requests using Alamofire
     - parameter method: The HTTP Methoud To Use
     - parameter url: is the url of service to call like login, register, getData in Type of String
     - parameter parameters: this is a Dictionary contains all data that you need to send this request
     - parameter header: is the authorization header attach with request
     - parameter closure: This is the final result that you can get data from request using it
     */
    class func sendRequest<T: Decodable>( userImage: Data? = nil, method: HTTPMethod, url: String, parameters:[String:Any]? = nil, header: [String:String]?  = nil, completion: @escaping (_ error: Error?, _ status: Bool?, _ response: T?)->Void) {
        
        if let image = userImage{
            //... Request With Image
            Alamofire.upload( multipartFormData: { multipartFormData in
                multipartFormData.append(image, withName: "img",fileName: "file.jpg", mimeType: "image/jpg")
                for (key, value) in parameters! {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                } //Optional for extra parameters
            }, to: url, method: method, headers:header){ (result) in
                
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print(response.result.value)
                        guard let json = response.result.value as? Dictionary<String, Any>  else { return }
                        if let _ = json["user"] as? Dictionary<String,Any>{
                            guard let data = response.data else { return }
                            do{
                                let response = try JSONDecoder().decode(T.self, from: data)
                                completion(nil,true,response)
                            }catch let err{
                                print("Error In Decode Data \(err.localizedDescription)")
                                completion(err,false,nil)
                            }
                        }else{
                            completion(nil,false,nil)
                        }
                        
                    }
                    
                case .failure(let encodingError):
                    completion(encodingError,false,nil)
                }
            }
            
        }else{
            //... Request Without Image
            NetworkingManager.shared.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header)
                .responseJSON { res -> Void in
                    print(res.result.value)
                    switch res.result
                    {
                    case .failure(let error):
                        completion(error,nil,nil)
                    case .success(_):
                        if let json = res.result.value as? Dictionary<String, Any>{
                            do{
                                if let _ = json["user"] as? Dictionary<String, Any> {
                                    guard let data = res.data else { return }
                                    let response = try JSONDecoder().decode(T.self, from: data)
                                    completion(nil,true,response)
                                }else if let _ = json["data"] as? [Dictionary<String,Any>]{ //for get categories
                                    guard let data = res.data else {return }
                                    let response = try JSONDecoder().decode(T.self, from: data)
                                    completion(nil,true,response)
                                }else{
                                    completion(nil,false,nil)
                                }
                            }catch let err{
                                print("Error In Decode Data \(err.localizedDescription)")
                                completion(err,false,nil)
                            }
                            
                        }
                    }
            }
        }
        
    }
    
    //MARK:- Load Location
    /**
     This Function is used as an abstract to handle API requests using Alamofire
     - parameter cityID: the ID of city to get the array of area of this city
     - parameter closure: This is the final result that you can get data from request using it..(Array of City or Array of Area)
     */
    class func load_location<T: Decodable>( cityID: Int? = nil, completion: @escaping (_ error: Error?, _ locationArray: [T]?)->Void) {
        
        var locationURL:String?
        if let id = cityID{
            locationURL = Constants.cities + "/" + "\(id)/areas"
        }else{
            locationURL = Constants.cities
        }
        
        NetworkingManager.shared.request(locationURL!, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).responseJSON { res -> Void in
            switch res.result
            {
            case .failure(let error):
                completion(error,nil)
            case .success(_):
                guard let data = res.data else { return }
                
                do{
                    let location = try JSONDecoder().decode([T].self, from: data)
                    completion(nil,location)
                }catch let err{
                    print("Error In Decode Data")
                    completion(err,nil)
                }
                
            }
            
        }
        
    }
    
    class func reset_Password( url: String, parameters: [String:Any], completion: @escaping (_ error: Error?, _ message: String?)->Void) {
        var message = "Success"
        NetworkingManager.shared.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .responseJSON { res -> Void in
                switch res.result
                {
                case .failure(let error):
                    completion(error,nil)
                case .success(_):
                    if let json = res.result.value as? Dictionary<String, Any>{
                        if let msg = json["errors"] as? String {
                            message = msg
                        }
                    }
                    completion(nil,message)
                }
        }
    }
    
    class func mark_Notification( url: String, method: HTTPMethod, completion: @escaping (_ status: Bool?)->Void) {
        NetworkingManager.shared.request(url, method: method, encoding: URLEncoding.default, headers: Constants.header)
            .responseJSON { res -> Void in
                print(res.result.value)
                guard let statusCode = res.response?.statusCode else {return}
                switch statusCode {
                case 200:
                    completion(true)
                default:
                    completion(false)
                }
        }
        
    }
    
    class func getProduct( id: Int, completion: @escaping (_ error: Error?, _ data:Product?)->Void) {
        let url = Constants.Products + "\(id)"
        
        NetworkingManager.shared.request(url, method: .get, encoding: URLEncoding.default, headers: nil)
            .responseJSON { res -> Void in
                
                switch res.result
                {
                    
                case .failure(let error):
                    completion(error,nil)
                    
                case .success(_):
                    
                    guard let data = res.data else { return }
                    
                    do{
                        let product = try JSONDecoder().decode(Product.self, from: data)
                        completion(nil,product)
                    }catch let err{
                        print("Error In Decode Data")
                        completion(err,nil)
                    }
                    
                }
                
        }
        
    }
    
}
