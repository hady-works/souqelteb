//
//  NetworkHelper.swift
//  souqelteb
//
//  Created by Mac on 12/3/19.
//  Copyright © 2019 Mac. All rights reserved.
//


import Alamofire
import MOLH

class NetworkHelper{
    
    static var accessToken: String?{
        didSet{
            UserDefaults.standard.set(accessToken, forKey: "accessToken")
        }
    }
    
    static var userEmail: String?{
        didSet{
            UserDefaults.standard.set(userEmail, forKey: "userEmail")
        }
    }
    
    static var favourite: [Int] = []{
        didSet{
            UserDefaults.standard.set(favourite, forKey: "favourite")
        }
    }
    
    static var cart: [Int] = []{
        didSet{
            UserDefaults.standard.set(cart, forKey: "cart")
        }
    }
    
    static var userImage: String?{
        didSet{
            UserDefaults.standard.set(userImage, forKey: "userImage")
        }
    }
    
    static var userName: String?{
        didSet{
            UserDefaults.standard.set(userName, forKey: "userName")
        }
    }
    
    static var userID: Int?{
        didSet{
            UserDefaults.standard.set(userID, forKey: "userID")
        }
    }
    
    static var city: Int?{
        didSet{
            UserDefaults.standard.set(city, forKey: "city")
        }
    }
    
    static var area: Int?{
        didSet{
            UserDefaults.standard.set(area, forKey: "area")
        }
    }
    
    static func getAccessToken() -> String? {
        if let accessToken = UserDefaults.standard.value(forKey: "accessToken") as? String{
            NetworkHelper.accessToken = accessToken
            print("accessToken: \(accessToken)")
        }
        return NetworkHelper.accessToken
    }
    
    static func getUserEmail() -> String? {
        if let userEmail = UserDefaults.standard.value(forKey: "userEmail") as? String{
            NetworkHelper.userEmail = userEmail
            print("userEmail: \(userEmail)")
        }
        return NetworkHelper.userEmail
    }
    
    static func getFavourite() -> [Int] {
        if let favourite = UserDefaults.standard.value(forKey: "favourite") as? [Int]{
            NetworkHelper.favourite = favourite
            print("favourite: \(favourite)")
        }
        return NetworkHelper.favourite
    }
    
    static func getCart() -> [Int] {
        if let cart = UserDefaults.standard.value(forKey: "cart") as? [Int]{
            NetworkHelper.cart = cart
            print("Cart: \(cart)")
        }
        return NetworkHelper.cart
    }
    
    static func getuserImage() -> String? {
        if let userImage = UserDefaults.standard.value(forKey: "userImage") as? String{
            NetworkHelper.userImage = userImage
            print("userImage: \(userImage)")
        }
        return NetworkHelper.userImage
    }
    
    static func getUserName() -> String? {
        if let userName = UserDefaults.standard.value(forKey: "userName") as? String{
            NetworkHelper.userName = userName
        }
        return NetworkHelper.userName
    }
    
    static func getUserId() -> Int? {
        if let userID = UserDefaults.standard.value(forKey: "userID") as? Int{
            NetworkHelper.userID = userID
        }
        return NetworkHelper.userID
    }
    
    static func getCityId() -> Int? {
        if let city = UserDefaults.standard.value(forKey: "city") as? Int{
            NetworkHelper.city = city
        }
        return NetworkHelper.city
    }
    
    static func getAreaId() -> Int? {
        if let area = UserDefaults.standard.value(forKey: "area") as? Int{
            NetworkHelper.area = area
        }
        return NetworkHelper.area
    }
    
    static func logoutAccessTokenCleanUp() {
        NetworkHelper.accessToken = nil
        UserDefaults.standard.removeObject(forKey: "accessToken")
    }
    
    static func logoutUserEmailCleanUp() {
        NetworkHelper.userEmail = nil
        UserDefaults.standard.removeObject(forKey: "userEmail")
    }
    
    static func logoutFavouriteCleanUp() {
        NetworkHelper.favourite = []
        UserDefaults.standard.removeObject(forKey: "favourite")
    }
    
    static func logoutCartCleanUp() {
        NetworkHelper.cart = []
        UserDefaults.standard.removeObject(forKey: "cart")
    }
    
    static func logoutCityCleanUp() {
        NetworkHelper.city = nil
        UserDefaults.standard.removeObject(forKey: "city")
    }
    
    static func logoutAreaCleanUp() {
        NetworkHelper.area = nil
        UserDefaults.standard.removeObject(forKey: "area")
    }
    
    static func logoutUserImageCleanUp() {
        NetworkHelper.userImage = nil
        UserDefaults.standard.removeObject(forKey: "userImage")
    }
    
    static func logoutUserNameCleanUp() {
        NetworkHelper.userName = nil
        UserDefaults.standard.removeObject(forKey: "userName")
    }
    
    static func logoutUserIdCleanUp() {
        NetworkHelper.userID = nil
        UserDefaults.standard.removeObject(forKey: "userID")
    }
    
    static func getHeaders() -> HTTPHeaders {
        if NetworkHelper.getAccessToken() != nil {
            var headers = ["Authorization": "Bearer \(NetworkHelper.getAccessToken()!)",
                "Content-Type":"application/json",
                "accept":"*/*",
                "accept-encoding":"gzip, deflate",
                "accept-language":"en"
                
                
            ]
            if MOLHLanguage.currentAppleLanguage() == "ar"{
                headers["LanguageCode"] = "Ar"
            }else{
                headers["LanguageCode"] = "En"
            }
            return headers
        }
        return [
            "Content-Type":"application/json",
            "accept":"*/*",
            "accept-encoding":"gzip, deflate",
            "accept-language":"en",
            "LanguageCode" : "En"
            
        ]
    }
    
}
