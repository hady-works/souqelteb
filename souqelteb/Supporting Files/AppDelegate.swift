//
//  AppDelegate.swift
//  souqelteb
//
//  Created by Mac on 12/3/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import CoreData
import MOLH
import SideMenu
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate{
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configSideMenu()
        if Constants.getIsLogIn(){
            //... Go To Main Screen
            window = UIWindow(frame: UIScreen.main.bounds)
            let mainVC = MainTabBar.instance()
            if MOLHLanguage.isArabic(){
                mainVC.tabBar.items?[0].title = "الصفحة الرئيسية"
                mainVC.tabBar.items?[1].title = "الإشعارات"
                mainVC.tabBar.items?[2].title = "الطلبات"
                mainVC.tabBar.items?[3].title = "الأقسام"
            }
            self.window?.rootViewController = mainVC
            self.window?.makeKeyAndVisible()
        }else{
            NetworkHelper.logoutFavouriteCleanUp()
            NetworkHelper.logoutCartCleanUp()
            NetworkHelper.logoutAccessTokenCleanUp()
        }
        
        //... Setup Navigation
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor(red: 52/255, green: 151/255, blue: 253/255, alpha: 1)
        UINavigationBar.appearance().isTranslucent = false
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        UIApplication.shared.statusBarStyle = .lightContent
        MOLH.shared.activate(true)
        self.attemptRegisterForNotifications(application: application)
        FirebaseApp.configure()
        return true
    }
    
    func configSideMenu() {
        let st = UIStoryboard(name: "Main", bundle: nil)
        let menu = st.instantiateViewController(withIdentifier: "MenuNavigation") as! UISideMenuNavigationController
        menu.menuWidth = (UIScreen.main.bounds.width) * 0.7
        if MOLHLanguage.currentAppleLanguage() == "ar"{
            SideMenuManager.default.menuRightNavigationController = menu
        }else {
            SideMenuManager.default.menuLeftNavigationController = menu
        }
        SideMenuManager.default.menuPresentMode = .viewSlideOut
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuAlwaysAnimate = true
        SideMenuManager.default.menuDismissWhenBackgrounded = true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        Messaging.messaging().shouldEstablishDirectChannel = false
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    private func configureRemoteNotification(application: UIApplication) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (success, error) in
            if success, error == nil {
                print("***** Sucess notification *****")
            } else {
                print("***** unsuccessful notification *****")
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
    }
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "souqelteb")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    /// method to get top UIViewController
    /// - Parameter controller: optional parameter UIViewController
    /// - Returns: UIViewController
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
}

extension AppDelegate:  MOLHResetable {
    
    func reset() {
        let rootViewController: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let mainVC = MainTabBar.instance()
        mainVC.modalPresentationStyle = .fullScreen
        rootViewController.rootViewController = mainVC
        self.configSideMenu()
    }
}

extension AppDelegate : MessagingDelegate, UNUserNotificationCenterDelegate {
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("Notification: APNs token: \((deviceToken as NSData))")
        print("Notification: APNs token retrieved: \(token)")
        
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        Messaging.messaging().shouldEstablishDirectChannel = true
        print("Refresh Registered with FCM with token", fcmToken)
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Registered with FCM with token", fcmToken)
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
    }
    
    func application(received remoteMessage: MessagingRemoteMessage) {
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        completionHandler(.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        let center = UNUserNotificationCenter.current()
        let content = UNMutableNotificationContent()
        if let data = userInfo["notification"] as? [String: String] {
            content.title = (data["title"] ?? "")
            content.body = (data["body"] ?? "")
            if let url = Bundle.main.url(forResource: "Logo", withExtension: "pdf") {
                let attachment = try! UNNotificationAttachment(identifier: "image", url: url, options: [:])
                content.attachments = [attachment]
            }
            content.sound = UNNotificationSound.default
            content.categoryIdentifier = "alarm"
        }
        var dateComponents = DateComponents()
        dateComponents.hour = 10
        dateComponents.minute = 30
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        center.add(request)
        
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        window = UIWindow(frame: UIScreen.main.bounds)
        let nav = UINavigationController(rootViewController: MainTabBar.instance())
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
        
    }
    
    // setup appDelegate for push notification's
    private func attemptRegisterForNotifications(application: UIApplication){
        
        Messaging.messaging().delegate = self
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        application.applicationIconBadgeNumber = 0
    }
    
}
